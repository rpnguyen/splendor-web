# splendor-web

> Web client to play [splendor board game](http://boardgamegeek.com/boardgame/148228/splendor)

## Getting Started

### Install node

[Node.js](http://nodejs.org/)

### Get dependencies

Populates folder `node_modules`

```
$ npm update
```

Populates folder `src/bower_components`
```
$ bower update
```

### Build

```
$ gulp development
```

```
$ gulp production
```

> Based on yeoman template [React Boilerplate](https://github.com/mitchbox/generator-react-boilerplate)