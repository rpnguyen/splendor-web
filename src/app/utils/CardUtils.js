'use strict';

module.exports = {

    lookup: function(id) {
        var card = _cards[id];
        card.id = id;
        return card;
    }

};

var _cards = {
    '0': {
        'level': 1,
        'points': 0,
        'colour': 'white',
        'cost': {
            'white': 0,
            'blue': 3,
            'green': 0,
            'red': 0,
            'black': 0
        }
    },
    '1': {
        'level': 1,
        'points': 0,
        'colour': 'white',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 0,
            'red': 2,
            'black': 1
        }
    },
    '2': {
        'level': 1,
        'points': 0,
        'colour': 'white',
        'cost': {
            'white': 0,
            'blue': 1,
            'green': 1,
            'red': 1,
            'black': 1
        }
    },
    '3': {
        'level': 1,
        'points': 0,
        'colour': 'white',
        'cost': {
            'white': 0,
            'blue': 2,
            'green': 0,
            'red': 0,
            'black': 2
        }
    },
    '4': {
        'level': 1,
        'points': 1,
        'colour': 'white',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 4,
            'red': 0,
            'black': 0
        }
    },
    '5': {
        'level': 1,
        'points': 0,
        'colour': 'white',
        'cost': {
            'white': 0,
            'blue': 1,
            'green': 2,
            'red': 1,
            'black': 1
        }
    },
    '6': {
        'level': 1,
        'points': 0,
        'colour': 'white',
        'cost': {
            'white': 0,
            'blue': 2,
            'green': 2,
            'red': 0,
            'black': 1
        }
    },
    '7': {
        'level': 1,
        'points': 0,
        'colour': 'white',
        'cost': {
            'white': 3,
            'blue': 1,
            'green': 0,
            'red': 0,
            'black': 1
        }
    },

    '8': {
        'level': 1,
        'points': 0,
        'colour': 'blue',
        'cost': {
            'white': 1,
            'blue': 0,
            'green': 0,
            'red': 0,
            'black': 2
        }
    },
    '9': {
        'level': 1,
        'points': 0,
        'colour': 'blue',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 0,
            'red': 0,
            'black': 3
        }
    },
    '10': {
        'level': 1,
        'points': 0,
        'colour': 'blue',
        'cost': {
            'white': 1,
            'blue': 0,
            'green': 1,
            'red': 1,
            'black': 1
        }
    },
    '11': {
        'level': 1,
        'points': 0,
        'colour': 'blue',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 2,
            'red': 0,
            'black': 2
        }
    },
    '12': {
        'level': 1,
        'points': 1,
        'colour': 'blue',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 0,
            'red': 4,
            'black': 0
        }
    },
    '13': {
        'level': 1,
        'points': 0,
        'colour': 'blue',
        'cost': {
            'white': 1,
            'blue': 0,
            'green': 1,
            'red': 2,
            'black': 1
        }
    },
    '14': {
        'level': 1,
        'points': 0,
        'colour': 'blue',
        'cost': {
            'white': 1,
            'blue': 0,
            'green': 2,
            'red': 2,
            'black': 0
        }
    },
    '15': {
        'level': 1,
        'points': 0,
        'colour': 'blue',
        'cost': {
            'white': 0,
            'blue': 1,
            'green': 3,
            'red': 1,
            'black': 0
        }
    },

    '16': {
        'level': 1,
        'points': 0,
        'colour': 'green',
        'cost': {
            'white': 2,
            'blue': 1,
            'green': 0,
            'red': 0,
            'black': 0
        }
    },
    '17': {
        'level': 1,
        'points': 0,
        'colour': 'green',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 0,
            'red': 3,
            'black': 0
        }
    },
    '18': {
        'level': 1,
        'points': 0,
        'colour': 'green',
        'cost': {
            'white': 1,
            'blue': 1,
            'green': 0,
            'red': 1,
            'black': 1
        }
    },
    '19': {
        'level': 1,
        'points': 0,
        'colour': 'green',
        'cost': {
            'white': 0,
            'blue': 2,
            'green': 0,
            'red': 2,
            'black': 0
        }
    },
    '20': {
        'level': 1,
        'points': 1,
        'colour': 'green',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 0,
            'red': 0,
            'black': 4
        }
    },
    '21': {
        'level': 1,
        'points': 0,
        'colour': 'green',
        'cost': {
            'white': 1,
            'blue': 1,
            'green': 0,
            'red': 1,
            'black': 2
        }
    },
    '22': {
        'level': 1,
        'points': 0,
        'colour': 'green',
        'cost': {
            'white': 0,
            'blue': 1,
            'green': 0,
            'red': 2,
            'black': 2
        }
    },
    '23': {
        'level': 1,
        'points': 0,
        'colour': 'green',
        'cost': {
            'white': 1,
            'blue': 3,
            'green': 1,
            'red': 0,
            'black': 0
        }
    },

    '24': {
        'level': 1,
        'points': 0,
        'colour': 'red',
        'cost': {
            'white': 0,
            'blue': 2,
            'green': 1,
            'red': 0,
            'black': 0
        }
    },
    '25': {
        'level': 1,
        'points': 0,
        'colour': 'red',
        'cost': {
            'white': 3,
            'blue': 0,
            'green': 0,
            'red': 0,
            'black': 0
        }
    },
    '26': {
        'level': 1,
        'points': 0,
        'colour': 'red',
        'cost': {
            'white': 1,
            'blue': 1,
            'green': 1,
            'red': 0,
            'black': 1
        }
    },
    '27': {
        'level': 1,
        'points': 0,
        'colour': 'red',
        'cost': {
            'white': 2,
            'blue': 0,
            'green': 0,
            'red': 2,
            'black': 0
        }
    },
    '28': {
        'level': 1,
        'points': 1,
        'colour': 'red',
        'cost': {
            'white': 4,
            'blue': 0,
            'green': 0,
            'red': 0,
            'black': 0
        }
    },
    '29': {
        'level': 1,
        'points': 0,
        'colour': 'red',
        'cost': {
            'white': 2,
            'blue': 1,
            'green': 1,
            'red': 0,
            'black': 1
        }
    },
    '30': {
        'level': 1,
        'points': 0,
        'colour': 'red',
        'cost': {
            'white': 2,
            'blue': 0,
            'green': 1,
            'red': 0,
            'black': 2
        }
    },
    '31': {
        'level': 1,
        'points': 0,
        'colour': 'red',
        'cost': {
            'white': 1,
            'blue': 0,
            'green': 0,
            'red': 1,
            'black': 3
        }
    },

    '32': {
        'level': 1,
        'points': 0,
        'colour': 'black',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 2,
            'red': 1,
            'black': 0
        }
    },
    '33': {
        'level': 1,
        'points': 0,
        'colour': 'black',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 3,
            'red': 0,
            'black': 0
        }
    },
    '34': {
        'level': 1,
        'points': 0,
        'colour': 'black',
        'cost': {
            'white': 1,
            'blue': 1,
            'green': 1,
            'red': 1,
            'black': 0
        }
    },
    '35': {
        'level': 1,
        'points': 0,
        'colour': 'black',
        'cost': {
            'white': 2,
            'blue': 0,
            'green': 2,
            'red': 0,
            'black': 0
        }
    },
    '36': {
        'level': 1,
        'points': 1,
        'colour': 'black',
        'cost': {
            'white': 0,
            'blue': 4,
            'green': 0,
            'red': 0,
            'black': 0
        }
    },
    '37': {
        'level': 1,
        'points': 0,
        'colour': 'black',
        'cost': {
            'white': 1,
            'blue': 2,
            'green': 1,
            'red': 1,
            'black': 0
        }
    },
    '38': {
        'level': 1,
        'points': 0,
        'colour': 'black',
        'cost': {
            'white': 2,
            'blue': 2,
            'green': 0,
            'red': 1,
            'black': 0
        }
    },
    '39': {
        'level': 1,
        'points': 0,
        'colour': 'black',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 1,
            'red': 3,
            'black': 1
        }
    },

    '40': {
        'level': 2,
        'points': 2,
        'colour': 'white',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 0,
            'red': 5,
            'black': 0
        }
    },
    '41': {
        'level': 2,
        'points': 3,
        'colour': 'white',
        'cost': {
            'white': 6,
            'blue': 0,
            'green': 0,
            'red': 0,
            'black': 0
        }
    },
    '42': {
        'level': 2,
        'points': 1,
        'colour': 'white',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 3,
            'red': 2,
            'black': 2
        }
    },
    '43': {
        'level': 2,
        'points': 2,
        'colour': 'white',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 1,
            'red': 4,
            'black': 2
        }
    },
    '44': {
        'level': 2,
        'points': 1,
        'colour': 'white',
        'cost': {
            'white': 2,
            'blue': 3,
            'green': 0,
            'red': 3,
            'black': 0
        }
    },
    '45': {
        'level': 2,
        'points': 2,
        'colour': 'white',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 0,
            'red': 5,
            'black': 3
        }
    },

    '46': {
        'level': 2,
        'points': 2,
        'colour': 'blue',
        'cost': {
            'white': 0,
            'blue': 5,
            'green': 0,
            'red': 0,
            'black': 0
        }
    },
    '47': {
        'level': 2,
        'points': 3,
        'colour': 'blue',
        'cost': {
            'white': 0,
            'blue': 6,
            'green': 0,
            'red': 0,
            'black': 0
        }
    },
    '48': {
        'level': 2,
        'points': 1,
        'colour': 'blue',
        'cost': {
            'white': 0,
            'blue': 2,
            'green': 2,
            'red': 3,
            'black': 0
        }
    },
    '49': {
        'level': 2,
        'points': 2,
        'colour': 'blue',
        'cost': {
            'white': 2,
            'blue': 0,
            'green': 0,
            'red': 1,
            'black': 4
        }
    },
    '50': {
        'level': 2,
        'points': 1,
        'colour': 'blue',
        'cost': {
            'white': 0,
            'blue': 2,
            'green': 3,
            'red': 0,
            'black': 3
        }
    },
    '51': {
        'level': 2,
        'points': 2,
        'colour': 'blue',
        'cost': {
            'white': 5,
            'blue': 3,
            'green': 0,
            'red': 0,
            'black': 0
        }
    },

    '52': {
        'level': 2,
        'points': 2,
        'colour': 'green',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 5,
            'red': 0,
            'black': 0
        }
    },
    '53': {
        'level': 2,
        'points': 3,
        'colour': 'green',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 6,
            'red': 0,
            'black': 0
        }
    },
    '54': {
        'level': 2,
        'points': 1,
        'colour': 'green',
        'cost': {
            'white': 2,
            'blue': 3,
            'green': 0,
            'red': 0,
            'black': 2
        }
    },
    '55': {
        'level': 2,
        'points': 2,
        'colour': 'green',
        'cost': {
            'white': 4,
            'blue': 2,
            'green': 0,
            'red': 0,
            'black': 1
        }
    },
    '56': {
        'level': 2,
        'points': 1,
        'colour': 'green',
        'cost': {
            'white': 3,
            'blue': 0,
            'green': 2,
            'red': 3,
            'black': 0
        }
    },
    '57': {
        'level': 2,
        'points': 2,
        'colour': 'green',
        'cost': {
            'white': 0,
            'blue': 5,
            'green': 3,
            'red': 0,
            'black': 0
        }
    },

    '58': {
        'level': 2,
        'points': 2,
        'colour': 'red',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 0,
            'red': 0,
            'black': 5
        }
    },
    '59': {
        'level': 2,
        'points': 3,
        'colour': 'red',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 0,
            'red': 6,
            'black': 0
        }
    },
    '60': {
        'level': 2,
        'points': 1,
        'colour': 'red',
        'cost': {
            'white': 2,
            'blue': 0,
            'green': 0,
            'red': 2,
            'black': 3
        }
    },
    '61': {
        'level': 2,
        'points': 2,
        'colour': 'red',
        'cost': {
            'white': 1,
            'blue': 4,
            'green': 2,
            'red': 0,
            'black': 0
        }
    },
    '62': {
        'level': 2,
        'points': 1,
        'colour': 'red',
        'cost': {
            'white': 0,
            'blue': 3,
            'green': 0,
            'red': 2,
            'black': 3
        }
    },
    '63': {
        'level': 2,
        'points': 2,
        'colour': 'red',
        'cost': {
            'white': 3,
            'blue': 0,
            'green': 0,
            'red': 0,
            'black': 5
        }
    },

    '64': {
        'level': 2,
        'points': 2,
        'colour': 'black',
        'cost': {
            'white': 5,
            'blue': 0,
            'green': 0,
            'red': 0,
            'black': 0
        }
    },
    '65': {
        'level': 2,
        'points': 3,
        'colour': 'black',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 0,
            'red': 0,
            'black': 6
        }
    },
    '66': {
        'level': 2,
        'points': 1,
        'colour': 'black',
        'cost': {
            'white': 3,
            'blue': 2,
            'green': 2,
            'red': 0,
            'black': 0
        }
    },
    '67': {
        'level': 2,
        'points': 2,
        'colour': 'black',
        'cost': {
            'white': 0,
            'blue': 1,
            'green': 4,
            'red': 2,
            'black': 0
        }
    },
    '68': {
        'level': 2,
        'points': 1,
        'colour': 'black',
        'cost': {
            'white': 3,
            'blue': 0,
            'green': 3,
            'red': 0,
            'black': 2
        }
    },
    '69': {
        'level': 2,
        'points': 2,
        'colour': 'black',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 5,
            'red': 3,
            'black': 0
        }
    },

    '70': {
        'level': 3,
        'points': 4,
        'colour': 'white',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 0,
            'red': 0,
            'black': 7
        }
    },
    '71': {
        'level': 3,
        'points': 5,
        'colour': 'white',
        'cost': {
            'white': 3,
            'blue': 0,
            'green': 0,
            'red': 0,
            'black': 7
        }
    },
    '72': {
        'level': 3,
        'points': 4,
        'colour': 'white',
        'cost': {
            'white': 3,
            'blue': 0,
            'green': 0,
            'red': 3,
            'black': 6
        }
    },
    '73': {
        'level': 3,
        'points': 3,
        'colour': 'white',
        'cost': {
            'white': 0,
            'blue': 3,
            'green': 3,
            'red': 5,
            'black': 3
        }
    },

    '74': {
        'level': 3,
        'points': 4,
        'colour': 'blue',
        'cost': {
            'white': 7,
            'blue': 0,
            'green': 0,
            'red': 0,
            'black': 0
        }
    },
    '75': {
        'level': 3,
        'points': 5,
        'colour': 'blue',
        'cost': {
            'white': 7,
            'blue': 3,
            'green': 0,
            'red': 0,
            'black': 0
        }
    },
    '76': {
        'level': 3,
        'points': 4,
        'colour': 'blue',
        'cost': {
            'white': 6,
            'blue': 3,
            'green': 0,
            'red': 0,
            'black': 3
        }
    },
    '77': {
        'level': 3,
        'points': 3,
        'colour': 'blue',
        'cost': {
            'white': 3,
            'blue': 0,
            'green': 3,
            'red': 3,
            'black': 5
        }
    },

    '78': {
        'level': 3,
        'points': 4,
        'colour': 'green',
        'cost': {
            'white': 0,
            'blue': 7,
            'green': 0,
            'red': 0,
            'black': 0
        }
    },
    '79': {
        'level': 3,
        'points': 5,
        'colour': 'green',
        'cost': {
            'white': 0,
            'blue': 7,
            'green': 3,
            'red': 0,
            'black': 0
        }
    },
    '80': {
        'level': 3,
        'points': 4,
        'colour': 'green',
        'cost': {
            'white': 3,
            'blue': 6,
            'green': 3,
            'red': 0,
            'black': 0
        }
    },
    '81': {
        'level': 3,
        'points': 3,
        'colour': 'green',
        'cost': {
            'white': 5,
            'blue': 3,
            'green': 0,
            'red': 3,
            'black': 3
        }
    },

    '82': {
        'level': 3,
        'points': 4,
        'colour': 'red',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 7,
            'red': 0,
            'black': 0
        }
    },
    '83': {
        'level': 3,
        'points': 5,
        'colour': 'red',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 7,
            'red': 3,
            'black': 0
        }
    },
    '84': {
        'level': 3,
        'points': 4,
        'colour': 'red',
        'cost': {
            'white': 0,
            'blue': 3,
            'green': 6,
            'red': 3,
            'black': 0
        }
    },
    '85': {
        'level': 3,
        'points': 3,
        'colour': 'red',
        'cost': {
            'white': 3,
            'blue': 5,
            'green': 3,
            'red': 0,
            'black': 3
        }
    },

    '86': {
        'level': 3,
        'points': 4,
        'colour': 'black',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 0,
            'red': 7,
            'black': 0
        }
    },
    '87': {
        'level': 3,
        'points': 5,
        'colour': 'black',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 0,
            'red': 7,
            'black': 3
        }
    },
    '88': {
        'level': 3,
        'points': 4,
        'colour': 'black',
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 3,
            'red': 6,
            'black': 3
        }
    },
    '89': {
        'level': 3,
        'points': 3,
        'colour': 'black',
        'cost': {
            'white': 3,
            'blue': 3,
            'green': 5,
            'red': 3,
            'black': 0
        }
    }
};