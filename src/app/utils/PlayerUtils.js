'use strict';

var CardUtils = require('./CardUtils');
var NobleUtils = require('./NobleUtils');
var Cookies = require('cookies-js');

var SplendorConstants = require('../constants/SplendorConstants');
var Keys = SplendorConstants.Keys;

module.exports = {

    convertPlayerData: function(rawPlayer, name, turnOrder) {
        return {
            name: name,
            turnOrder: turnOrder,
            cards: (rawPlayer.cards) ? rawPlayer.cards.map(CardUtils.lookup) : [],
            reservedCards: (rawPlayer.reservedCards) ? rawPlayer.reservedCards.map(CardUtils.lookup) : [],
            nobles: (rawPlayer.nobles) ? rawPlayer.nobles.map(NobleUtils.lookup) : [],
            tokens: rawPlayer.tokens
        };
    },

    getUsername: function() {
        return Cookies.get(Keys.USERNAME);
    },

    isUser: function(playerName) {
        return playerName === this.getUsername();
    },

    calculateScore: function(player) {
        var cardScore = (player.cards) ?
            player.cards.map(function(card) {
                return card.points;
            }).reduce(function(a, b) {
                return (b === undefined) ? a : a + b;
            }, 0)
            : 0;
        var noblesScore = (player.nobles) ?
            player.nobles.map(function(noble) {
                return noble.points;
            }).reduce(function(a, b) {
                return (b === undefined) ? a : a + b;
            }, 0)
            : 0;
        return cardScore + noblesScore;
    },

    canAffordWithCardsAndTokens: function(cards, tokens, cost) {
        var totalBudget = this.calculateTotalBudget(cards, tokens);
        return this.budgetExceedsOrEqualsCost(totalBudget, cost);
    },

    calculateTotalBudget: function(cards, tokens) {
        var buyingPowerOfEachColour = JSON.parse(JSON.stringify(tokens)); // clone object
        for (var card of cards) {
            buyingPowerOfEachColour[card.colour]++;
        }
        return buyingPowerOfEachColour;
    },

    budgetExceedsOrEqualsCost: function(budget, cost) {
        return this.calculateDeficit(budget, cost) <= budget.gold;
    },

    calculateDeficit: function(budget, cost) {
        var deficit = 0;
        deficit += Math.min(budget.white - cost.white, 0);
        deficit += Math.min(budget.blue - cost.blue, 0);
        deficit += Math.min(budget.green - cost.green, 0);
        deficit += Math.min(budget.red - cost.red, 0);
        deficit += Math.min(budget.black - cost.black, 0);
        return Math.abs(deficit);
    }

};