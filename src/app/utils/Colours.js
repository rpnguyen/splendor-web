'use strict';

module.exports = {

    toHex: function(colour) {
        return _colours[colour];
    }

};

var _colours = {
    'red': '#F08030',
    'green': '#78C850',
    'blue': '#6890F0',
    'white': '#dcdca5',
    'black': '#705848',
    'gold': '#F8D030'
};