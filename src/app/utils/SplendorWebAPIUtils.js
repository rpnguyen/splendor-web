'use strict';

var SplendorServerActionCreators = require('../actions/SplendorServerActionCreators');
var request = require('superagent');
var PlayerUtils = require('../utils/PlayerUtils');

module.exports = {

    baseUrl: function(path) {
        return 'http://frozen-cove-7242.herokuapp.com/' + path;
    },

    getDataFromServer: function() {
        var uri = this.baseUrl('status') + '?username=' + PlayerUtils.getUsername();
        request.get(uri, function(res){
                SplendorServerActionCreators.gameSync(res.body);
            // TODO on failure, retry
        });

        //var Cookies = require('cookies-js');
        //Cookies.set('USERNAME', 'Richard');
        //SplendorServerActionCreators.gameSync(initialData);
    },

    postToServer: function(path, data) {
        var uri = this.baseUrl(path);
        request.post(uri)
            .send(data)
            .end(function(res) {
                if (res.ok) {
                    SplendorServerActionCreators.endTurn();
                    SplendorServerActionCreators.gameSync(res.body);
                } else {
                    console.log('something went wrong posting ' + JSON.stringify(data) + ' to ' + uri);
                }
            });
        // TODO on failure, retry
    }

};

var initialData = {
    'currentPlayer': 'Richard',
    'playerList': [
        'Richard',
        'Lizzy',
        'Chris',
        'Joy'
    ],
    'shared': {
        'cards': [0, 32, 2, 15, 40, 55, 50, 69, 84, 80, 85, 89],
        'remaining': {
            '1': 40,
            '2': 30,
            '3': 20
        },
        'nobles': [0, 3, 2, 7, 9],
        'tokens': {
            'green': 7,
            'white': 2,
            'blue': 7,
            'black': 0,
            'red': 3,
            'gold': 3
        }
    },
    'players': {
        'Richard': {
            'cards': [2, 3, 7, 8, 13, 16, 17, 25, 26, 27, 32, 33, 34, 56, 58],
            'reservedCards': [52, 83],
            'tokens': {
                'green': 3,
                'white': 4,
                'blue': 0,
                'black': 1,
                'red': 0,
                'gold': 2
            },
            'nobles': []
        },
        'Lizzy': {
            'cards': [7, 8, 9],
            'reservedCards': [],
            'tokens': {
                'green': 0,
                'white': 1,
                'blue': 0,
                'black': 2,
                'red': 4,
                'gold': 0
            },
            'nobles': []
        },
        'Chris': {
            'cards': [],
            'reservedCards': [5, 42, 81],
            'tokens': {
                'green': 0,
                'white': 0,
                'blue': 0,
                'black': 2,
                'red': 0,
                'gold': 0
            },
            'nobles': []
        },
        'Joy': {
            'cards': [23, 24, 25],
            'reservedCards': [1, 2],
            'tokens': {
                'green': 0,
                'white': 0,
                'blue': 0,
                'black': 2,
                'red': 0,
                'gold': 0
            },
            'nobles': []
        }
    }
};
