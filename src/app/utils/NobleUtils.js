'use strict';

module.exports = {

    lookup: function(id) {
        var noble = _nobles[id];
        noble.id = id;
        return noble;
    }

};

var _nobles = {
    '0': {
        'points': 3,
        'cost': {
            'white': 3,
            'blue': 3,
            'green': 0,
            'red': 0,
            'black': 3
        }
    },
    '1': {
        'points': 3,
        'cost': {
            'white': 0,
            'blue': 3,
            'green': 3,
            'red': 3,
            'black': 0
        }
    },
    '2': {
        'points': 3,
        'cost': {
            'white': 3,
            'blue': 0,
            'green': 0,
            'red': 3,
            'black': 3
        }
    },
    '3': {
        'points': 3,
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 4,
            'red': 4,
            'black': 0
        }
    },
    '4': {
        'points': 3,
        'cost': {
            'white': 0,
            'blue': 4,
            'green': 4,
            'red': 0,
            'black': 0
        }
    },
    '5': {
        'points': 3,
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 0,
            'red': 4,
            'black': 4
        }
    },
    '6': {
        'points': 3,
        'cost': {
            'white': 4,
            'blue': 0,
            'green': 0,
            'red': 0,
            'black': 4
        }
    },
    '7': {
        'points': 3,
        'cost': {
            'white': 3,
            'blue': 3,
            'green': 3,
            'red': 0,
            'black': 0
        }
    },
    '8': {
        'points': 3,
        'cost': {
            'white': 0,
            'blue': 0,
            'green': 3,
            'red': 3,
            'black': 3
        }
    },
    '9': {
        'points': 3,
        'cost': {
            'white': 4,
            'blue': 4,
            'green': 0,
            'red': 0,
            'black': 0
        }
    }
};