'use strict';

var SplendorConstants = require('../constants/SplendorConstants');
var ActionTypes = SplendorConstants.ActionTypes;
var PlayerUtils = require('../utils/PlayerUtils');

module.exports = {

    inTakeTokensPhase: function(turnData) {
        return turnData.action === ActionTypes.TAKE_TOKENS_PHASE;
    },

    canTakeTokens: function(turnData, tokens) {
        var tokensTaken = 0;
        var emptyStacks = 0;
        for (var colour in turnData.tokens) {
            if (turnData.tokens[colour] === 2) return false;

            if (turnData.tokens[colour] === 1) {
                tokensTaken++;
            } else {
                // In case there are only 2 stacks available
                // We want this to bail after 2 tokens taken
                if (colour !== 'gold'
                    && (tokens[colour] === 0 // this stack is empty
                    || turnData.discard[colour] === tokens[colour])  // A colour that subsequently had tokens discarded to it can't be considered "empty"
                )
                    emptyStacks++;
            }
        }
        return tokensTaken < 3 && (tokensTaken + emptyStacks < 5);
    },

    canTakeColour: function(turnData, colour, tokenCount) {
        var tokensTaken = 0;
        for (var col in turnData.tokens) {
            tokensTaken += turnData.tokens[col];
        }

        if (colour === 'gold') return false;
        if (turnData.tokens && turnData.tokens[colour] === 1 && tokenCount < 3) return false; // Can't take two of same if <4 in stack
        if (turnData.tokens && turnData.tokens[colour] === 1 && tokensTaken >= 2) return false; // Can't take two of same if taking 3 different colours

        return true;
    },

    needsToDiscard: function(tokens) {
        var count = 0;
        for (var col in tokens) {
            count += tokens[col];
        }
        return count > 10;
    },


    inBuyCardPhase: function(turnData) {
        return turnData.action === ActionTypes.BUY_CARD_PHASE;
    },

    needsToChooseCard: function(turnData) {
        return turnData.card === null;
    },

    needsToPay: function(turnData, player) {
        return !PlayerUtils.canAffordWithCardsAndTokens(player.cards, turnData.pay, turnData.card.cost);
    },

    paymentNeedsColour: function(turnData, cards, colour) {
        if (colour === 'gold') return true;

        var cardBonusesForThisColour = 0;
        for (var card of cards) {
            if (card.colour === colour) {
                cardBonusesForThisColour++;
            }
        }
        return turnData.card.cost[colour] > cardBonusesForThisColour + turnData.pay[colour];
    },

    needsToTakeNoble: function(turnData, cards, nobles) {
        if (turnData.noble !== null) return false;

        for (var noble of nobles) {
            if (this.canAffordNoble(turnData, cards, noble)) {
                return true;
            }
        }
        return false;
    },

    canAffordNoble: function(turnData, cards, noble) {
        var cardBonus = {
            'red': 0,
            'green': 0,
            'blue': 0,
            'white': 0,
            'black': 0
        };
        for (var card of cards) {
            cardBonus[card.colour]++;
        }

        var deficit = 0;
        deficit += Math.min(cardBonus.white - noble.cost.white, 0);
        deficit += Math.min(cardBonus.blue - noble.cost.blue, 0);
        deficit += Math.min(cardBonus.green - noble.cost.green, 0);
        deficit += Math.min(cardBonus.red - noble.cost.red, 0);
        deficit += Math.min(cardBonus.black - noble.cost.black, 0);
        return deficit === 0;
    },


    inReservationPhase: function(turnData) {
        return turnData.action === ActionTypes.RESERVATION_PHASE;
    },

    canReserveCard: function(turnData, player) {
        return turnData.card === null && turnData.deck === null && player.reservedCards.length < 3;
    },

    needsToTakeGold: function(turnData, tokens) {
        return turnData.tokens.gold === 0 && tokens.gold > 0;
    },


    gameComplete: function(turnData) {
        return turnData.action === ActionTypes.END_GAME;
    },


    getUrl: function(turnData) {
        var url;
        switch (turnData.action) {
            case ActionTypes.TAKE_TOKENS_PHASE:
                url = 'takeTokens';
                break;

            case ActionTypes.BUY_CARD_PHASE:
                url = 'buyCard';
                break;

            case ActionTypes.RESERVATION_PHASE:
                url = 'reserveCard';
                break;
        }
        return url;
    },

    createPayload: function(turnData) {
        var payload;
        switch (turnData.action) {
            case ActionTypes.TAKE_TOKENS_PHASE:
                payload = this._takeTokensPayload(turnData);
                break;

            case ActionTypes.BUY_CARD_PHASE:
                payload = this._buyCardPayload(turnData);
                break;

            case ActionTypes.RESERVATION_PHASE:
                payload = this._reservationPayload(turnData);
                break;
        }
        return payload;
    },

    _takeTokensPayload: function(turnData) {
        return {
            username: PlayerUtils.getUsername(),
            tokens: turnData.tokens,
            discard: turnData.discard,
            noble: turnData.noble ? turnData.noble.id : null
        };
    },

    _buyCardPayload: function(turnData) {
        return {
            username: PlayerUtils.getUsername(),
            card: turnData.card.id,
            noble: turnData.noble ? turnData.noble.id : null,
            pay: turnData.pay
        };
    },

    _reservationPayload: function(turnData) {
        return {
            username: PlayerUtils.getUsername(),
            deck: turnData.deck ? turnData.deck : null,
            card: turnData.card ? turnData.card.id : null,
            tokens: turnData.tokens,
            discard: turnData.discard,
            noble: turnData.noble ? turnData.noble.id : null
        };
    }

};