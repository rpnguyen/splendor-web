'use strict';

var React = require('react');
var Cookies = require('cookies-js');

var Router = require('react-router');
var Route = Router.Route;
var NotFoundRoute = Router.NotFoundRoute;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var RouteHandler = Router.RouteHandler;

var HomePage = require('./components/HomePage.react');
var HelpPage = require('./components/HelpPage.react');
var LobbyPage = require('./components/LobbyPage.react');
var GamePage = require('./components/GamePage.react');

var App = React.createClass({
    render: function() {
        return (
            <div>

                <nav className="navbar navbar-default navbar-static-top" role="navigation">
                    <div className="container">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                            <a className="navbar-brand" href="#">PokéSplendor
                                <sup>βeta</sup>
                            </a>
                        </div>
                        <div id="navbar" className="navbar-collapse collapse">
                            <ul className="nav navbar-nav navbar-right">
                                <li>
                                    <Link to="home">Home</Link>
                                </li>
                                <li>
                                    <Link to="help">How to play</Link>
                                </li>
                                <li>
                                    <Link to="lobby">Find/create game</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>


                <div className="container">
                    <RouteHandler/>
                </div>
            </div>
        );
    }
});

var routes = (
    <Route handler={App}>
        <Route name="help" handler={HelpPage} />
        <Route name="lobby" handler={LobbyPage} />
        <Route name="game" handler={GamePage} />
        <DefaultRoute name="home" handler={HomePage}/>
    </Route>
);

Router.run(routes, function(Handler) {
    React.render(<Handler/>, document.getElementById('app'));
});