'use strict';

var keyMirror = require('keymirror');

module.exports = {

    ActionTypes: keyMirror({
        GAME_SYNC: null,

        START_TURN: null,

        TAKE_TOKENS_PHASE: null,
        TAKE_TOKEN: null,
        RETURN_TOKEN: null,

        BUY_CARD_PHASE: null,
        BUY_CARD: null,
        PAY_TOKEN: null,
        TAKE_NOBLE: null,

        RESERVATION_PHASE: null,
        RESERVE_CARD: null,
        RESERVE_DECK: null,

        UNDO: null,
        END_TURN: null,

        END_GAME: null
    }),

    PayloadSources: keyMirror({
        SERVER_ACTION: null,
        VIEW_ACTION: null
    }),

    Keys: keyMirror({
        USERNAME: null
    })

};
