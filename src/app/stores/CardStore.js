'use strict';

var AppDispatcher = require('../dispatcher/AppDispatcher');
var SplendorConstants = require('../constants/SplendorConstants');
var CardUtils = require('../utils/CardUtils');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var ActionTypes = SplendorConstants.ActionTypes;
var CHANGE_EVENT = 'change';

var _cardsUndo = {};
var _cards = [];
var _deckCountsUndo = {};
var _deckCounts = {};

var CardStore = assign({}, EventEmitter.prototype, {

    getAll: function() {
        return _cards;
    },

    getDeckCounts: function() {
        return _deckCounts;
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

CardStore.dispatchToken = AppDispatcher.register(function(payload) {
    var action = payload.action;

    switch (action.type) {
        case ActionTypes.GAME_SYNC:
            _cards = action.data.shared.cards ? action.data.shared.cards.map(CardUtils.lookup) : [];
            _deckCounts = action.data.shared.remaining;
            break;

        case ActionTypes.BUY_CARD:
        case ActionTypes.RESERVE_CARD:
            if (_cards.indexOf(action.card) > -1) {
                var placeHolder = JSON.parse(JSON.stringify(_cards[_cards.indexOf(action.card)]));
                placeHolder.id = -1;
                _cards[_cards.indexOf(action.card)] = placeHolder;
            }
            break;

        case ActionTypes.RESERVE_DECK:
            _deckCounts[action.level]--;
            break;

        case ActionTypes.START_TURN:
            _cardsUndo = JSON.parse(JSON.stringify(_cards));
            _deckCountsUndo = JSON.parse(JSON.stringify(_deckCounts));
            break;

        case ActionTypes.UNDO:
            _cards = JSON.parse(JSON.stringify(_cardsUndo));
            _deckCounts = JSON.parse(JSON.stringify(_deckCountsUndo));
            break;

        default:
            return true;
    }

    CardStore.emitChange();

    return true; // No errors.  Needed by promise in Dispatcher.
});

module.exports = CardStore;
