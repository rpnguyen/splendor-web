'use strict';

var AppDispatcher = require('../dispatcher/AppDispatcher');
var TurnStore = require('./TurnStore');
var TurnUtils = require('../utils/TurnUtils');
var PlayerUtils = require('../utils/PlayerUtils');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var SplendorConstants = require('../constants/SplendorConstants');
var ActionTypes = SplendorConstants.ActionTypes;
var CHANGE_EVENT = 'change';

var _playersUndo = [];
var _players = [];
var _playerList = [];

var PlayerStore = assign({}, EventEmitter.prototype, {

    getAll: function() {
        return _players;
    },

    getPlayerCount: function() {
        return _playerList.length;
    },

    getTurnIndex: function(playerName) {
        return _playerList.indexOf(playerName);
    },

    getPlayer: function(playerName) {
        return _players[_playerList.indexOf(playerName)];
    },

    getUser: function() {
        return this.getPlayer(PlayerUtils.getUsername());
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

function awardCardIfEligible() {
    var turnData = TurnStore.getTurnData();
    var user = PlayerStore.getUser();
    if (!TurnUtils.needsToPay(turnData, user)) { // YAY we paid the card off! We can add it to our hand now

        // Ensure we don't double-add this card
        if (user.cards.indexOf(turnData.card) === -1) {
            user.cards.push(turnData.card);
        }
        // Also, remove it from this user's reserved cards
        var reservedCards = PlayerStore.getUser().reservedCards;
        if (reservedCards.indexOf(turnData.card) > -1) {
            reservedCards.splice(reservedCards.indexOf(turnData.card), 1);
        }
    }
}

PlayerStore.dispatchToken = AppDispatcher.register(function(payload) {
    var action = payload.action;

    switch (action.type) {
        case ActionTypes.GAME_SYNC:
            _playerList = action.data.playerList;
            _players = [];
            for (var playerName of _playerList) {
                _players.push(PlayerUtils.convertPlayerData(action.data.players[playerName], playerName, _playerList.indexOf(playerName) + 1));
            }
            break;

        case ActionTypes.START_TURN:
            _playersUndo = JSON.parse(JSON.stringify(_players));
            break;

        case ActionTypes.UNDO:
            _players = JSON.parse(JSON.stringify(_playersUndo));
            break;

        case ActionTypes.TAKE_TOKEN:
            PlayerStore.getUser().tokens[action.colour]++;
            break;

        case ActionTypes.RETURN_TOKEN:
            PlayerStore.getUser().tokens[action.colour]--;
            break;

        case ActionTypes.BUY_CARD:
            AppDispatcher.waitFor([TurnStore.dispatchToken]);
            awardCardIfEligible();
            break;

        case ActionTypes.PAY_TOKEN:
            PlayerStore.getUser().tokens[action.colour]--;
            AppDispatcher.waitFor([TurnStore.dispatchToken]);
            awardCardIfEligible();
            break;

        case ActionTypes.TAKE_NOBLE:
            PlayerStore.getUser().nobles.push(action.noble);
            break;

        case ActionTypes.RESERVE_CARD:
            PlayerStore.getUser().reservedCards.push(action.card);
            break;

        case ActionTypes.RESERVE_DECK:
            PlayerStore.getUser().reservedCards.push({id: -1, level: action.level});
            break;

        default:
            return true;
    }

    PlayerStore.emitChange();

    return true; // No errors.  Needed by promise in Dispatcher.
});

module.exports = PlayerStore;
