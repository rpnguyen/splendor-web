'use strict';

var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var SplendorConstants = require('../constants/SplendorConstants');
var ActionTypes = SplendorConstants.ActionTypes;
var CHANGE_EVENT = 'change';

var _currentPlayerName = '';
var _turnData = {};

var TurnStore = assign({}, EventEmitter.prototype, {

    init: function() {
        _turnData = {};
    },

    getCurrentPlayerName: function() {
        return _currentPlayerName;
    },

    getTurnData: function() {
        return _turnData;
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

TurnStore.dispatchToken = AppDispatcher.register(function(payload) {
    var action = payload.action;

    switch (action.type) {
        case ActionTypes.GAME_SYNC:
            _currentPlayerName = action.data.currentPlayer;
            break;

        case ActionTypes.START_TURN:
        case ActionTypes.UNDO:
            TurnStore.init();
            _turnData.action = ActionTypes.START_TURN;
            break;

        case ActionTypes.TAKE_TOKENS_PHASE:
            _turnData = {
                'action': ActionTypes.TAKE_TOKENS_PHASE,
                'tokens': {
                    'green': 0,
                    'white': 0,
                    'blue': 0,
                    'black': 0,
                    'red': 0
                },
                'discard': {
                    'green': 0,
                    'white': 0,
                    'blue': 0,
                    'black': 0,
                    'red': 0,
                    'gold': 0
                },
                'noble': null
            };
            break;

        case ActionTypes.BUY_CARD_PHASE:
            _turnData = {
                'action': ActionTypes.BUY_CARD_PHASE,
                'card': null,
                'pay': {
                    'green': 0,
                    'white': 0,
                    'blue': 0,
                    'black': 0,
                    'red': 0,
                    'gold': 0
                },
                'noble': null
            };
            break;

        case ActionTypes.RESERVATION_PHASE:
            _turnData = {
                'action': ActionTypes.RESERVATION_PHASE,
                'deck': null,
                'card': null,
                'tokens': {
                    'gold': 0
                },
                'discard': {
                    'green': 0,
                    'white': 0,
                    'blue': 0,
                    'black': 0,
                    'red': 0,
                    'gold': 0
                },
                'noble': null
            };
            break;

        case ActionTypes.TAKE_TOKEN:
            _turnData.tokens[action.colour]++;
            break;

        case ActionTypes.RETURN_TOKEN:
            _turnData.discard[action.colour]++;
            break;

        case ActionTypes.BUY_CARD:
            _turnData.card = action.card;
            break;

        case ActionTypes.PAY_TOKEN:
            _turnData.pay[action.colour]++;
            break;

        case ActionTypes.TAKE_NOBLE:
            _turnData.noble = action.noble;
            break;

        case ActionTypes.RESERVE_CARD:
            _turnData.card = action.card;
            break;

        case ActionTypes.RESERVE_DECK:
            _turnData.deck = action.level;
            break;

        case ActionTypes.END_TURN:
            TurnStore.init();
            break;

        case ActionTypes.END_GAME:
            _turnData = {
                'action': ActionTypes.END_GAME
            };
            break;

        default:
            return true;
    }

    TurnStore.emitChange();

    return true; // No errors.  Needed by promise in Dispatcher.
});

module.exports = TurnStore;
