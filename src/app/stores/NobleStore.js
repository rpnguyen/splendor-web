'use strict';

var AppDispatcher = require('../dispatcher/AppDispatcher');
var SplendorConstants = require('../constants/SplendorConstants');
var NobleUtils = require('../utils/NobleUtils');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var ActionTypes = SplendorConstants.ActionTypes;
var CHANGE_EVENT = 'change';

var _noblesUndo = [];
var _nobles = [];

var NobleStore = assign({}, EventEmitter.prototype, {

    getAll: function() {
        return _nobles;
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

NobleStore.dispatchToken = AppDispatcher.register(function(payload) {
    var action = payload.action;

    switch (action.type) {
        case ActionTypes.GAME_SYNC:
            _nobles = action.data.shared.nobles ? action.data.shared.nobles.map(NobleUtils.lookup) : [];
            break;

        case ActionTypes.TAKE_NOBLE:
            _nobles.splice(_nobles.indexOf(action.noble), 1);
            break;

        case ActionTypes.START_TURN:
            _noblesUndo = JSON.parse(JSON.stringify(_nobles));
            break;

        case ActionTypes.UNDO:
            _nobles = JSON.parse(JSON.stringify(_noblesUndo));
            break;

        default:
            return true;
    }

    NobleStore.emitChange();

    return true; // No errors.  Needed by promise in Dispatcher.
});

module.exports = NobleStore;
