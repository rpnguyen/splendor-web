'use strict';

var AppDispatcher = require('../dispatcher/AppDispatcher');
var SplendorConstants = require('../constants/SplendorConstants');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var ActionTypes = SplendorConstants.ActionTypes;
var CHANGE_EVENT = 'change';

var _tokensUndo = {};
var _tokens = {};

var TokenStore = assign({}, EventEmitter.prototype, {

    getAll: function() {
        return _tokens;
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

TokenStore.dispatchToken = AppDispatcher.register(function(payload) {
    var action = payload.action;

    switch (action.type) {
        case ActionTypes.GAME_SYNC:
            _tokens = action.data.shared.tokens;
            break;

        case ActionTypes.START_TURN:
            _tokensUndo = JSON.parse(JSON.stringify(_tokens));
            break;

        case ActionTypes.UNDO:
            _tokens = JSON.parse(JSON.stringify(_tokensUndo));
            break;

        case ActionTypes.TAKE_TOKEN:
            _tokens[action.colour]--;
            break;

        case ActionTypes.RETURN_TOKEN:
        case ActionTypes.PAY_TOKEN:
            _tokens[action.colour]++;
            break;

        default:
            return true;
    }

    TokenStore.emitChange();

    return true; // No errors.  Needed by promise in Dispatcher.
});

module.exports = TokenStore;
