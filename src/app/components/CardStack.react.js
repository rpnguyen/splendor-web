'use strict';

var Card = require('./Card.react');
var React = require('react');

var ReactPropTypes = React.PropTypes;

var CardStack = React.createClass({

    getDefaultProps: function() {
        return {
            colour: '',
            cards: []
        };
    },

    propTypes: {
        colour: ReactPropTypes.string,
        cards: ReactPropTypes.array
    },

    render: function() {
        var stack = [];
        for (var card of this.props.cards) {
            if (card.colour === this.props.colour) {
                stack.push(
                    <Card key={card.id} card={card} />
                );
            }
        }
        return (
            <div className="btn-group-vertical card-stack">
            {stack}
            </div>
        );
    }

});

module.exports = CardStack;
