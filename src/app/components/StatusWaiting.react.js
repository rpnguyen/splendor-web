'use strict';

var React = require('react');

var ReactPropTypes = React.PropTypes;

var StatusWaiting = React.createClass({

    getDefaultProps: function() {
        return {
            turnData: {},
            currentPlayerName: ''
        };
    },

    propTypes: {
        turnData: ReactPropTypes.object,
        currentPlayerName: ReactPropTypes.string
    },

    render: function() {
        var status;
        var turnData = this.props.turnData;
        if (turnData.action === undefined) {
            status = (
                <span>
                    <span className="fa fa-refresh fa-spin"></span> Waiting... Current player is {this.props.currentPlayerName}
                </span>
            );
        } else {
            status = <span />
        }
        return status;
    }

});

module.exports = StatusWaiting;
