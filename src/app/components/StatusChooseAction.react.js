'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var React = require('react');

var SplendorConstants = require('../constants/SplendorConstants');
var ActionTypes = SplendorConstants.ActionTypes;

var ReactPropTypes = React.PropTypes;

var StatusChooseAction = React.createClass({

    getDefaultProps: function() {
        return {
            turnData: {},
            user: {
                cards: [],
                reservedCards: [],
                tokens: {},
                nobles: []
            }
        };
    },

    propTypes: {
        turnData: ReactPropTypes.object,
        user: ReactPropTypes.object
    },

    render: function() {
        var status;
        var turnData = this.props.turnData;
        var canReserve = this.props.user.reservedCards.length < 3;
        if (turnData.action === ActionTypes.START_TURN) {
            status = (
                <div className="clearfix">
                    <div className="btn-group pull-right" role="group">
                        <button type="button" className="btn btn-default" onClick={SplendorViewActionCreators.startTakeTokens}>
                            <span className="fa fa-dollar"></span> Take gems
                        </button>
                        <button type="button" className="btn btn-default" onClick={SplendorViewActionCreators.startBuyCard}>
                            <span className="fa fa-shopping-cart"></span> Buy card
                        </button>
                        <button type="button" disabled={!canReserve} className="btn btn-default" onClick={SplendorViewActionCreators.startReservation}>
                            <span className="fa fa-lock"></span> Reserve card
                        </button>
                    </div>
                    You're up!
                </div>
            );
        } else {
            status = <span />;
        }
        return status;
    }

});

module.exports = StatusChooseAction;
