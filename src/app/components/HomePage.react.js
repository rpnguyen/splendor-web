'use strict';

var React = require('react');

var HomePage = React.createClass({
    render: function() {
        return (
            <div className="jumbotron">
                <h1>PokéSplendor
                    <sup>βeta</sup>
                </h1>
                <p>Online multiplayer adaptation of the card game <a href="http://www.spacecowboys.fr/splendor/language:eng">Splendor</a></p>
                <p>Built using <a href="http://projects.spring.io/spring-boot/">Spring Boot</a>, <a href="http://facebook.github.io/flux/">Flux</a>,
                    and <a href="http://facebook.github.io/react/">React</a> by those guys <a href="http://au.linkedin.com/in/rpnguyen">Richard</a> and <a href="http://au.linkedin.com/in/wenfengxiao">Wen</a></p>
                <p>
                    <a className="btn btn-lg btn-primary" href="#/lobby" role="button">Find or create a game »</a>
                </p>
            </div>
        );
    }

});

module.exports = HomePage;