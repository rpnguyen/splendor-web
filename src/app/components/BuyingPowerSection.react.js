'use strict';

var React = require('react');
var PlayerUtils = require('../utils/PlayerUtils');

var ReactPropTypes = React.PropTypes;

var BuyingPowerSection = React.createClass({

    getDefaultProps: function() {
        return {
            player: {}
        };
    },

    propTypes: {
        player: ReactPropTypes.object
    },

    render: function() {
        var costBubbles = [];
        var budget = PlayerUtils.calculateTotalBudget(this.props.player.cards, this.props.player.tokens);
        for (var colour of ['white', 'blue', 'green', 'red', 'black', 'gold']) { // Ordered manually so the colours show up in the correct sequence
            if (budget[colour] > 0) {
                costBubbles.push(
                    <div className={'disabled btn btn-default cost-bubble ' + colour} key={colour}>
                        {budget[colour]}
                    </div>
                );
            }
        }

        return (
            <div className="cost btn-group-vertical" role="group">
                {costBubbles}
            </div>
        );
    }

});

module.exports = BuyingPowerSection;
