'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var React = require('react');

var ReactPropTypes = React.PropTypes;

var CreateGame = React.createClass({

    getInitialState: function() {
        return {
            gameName: '',
            players: []
        };
    },

    render: function() {
        return (
            <form role="form">
                <fieldset disabled={true}>
                    <div className="form-group">
                        <label>Game name</label>
                        <input type="text" className="form-control" id="game-name" placeholder="My PokéSplendor Game" />
                    </div>
                    <div className="form-group">
                        <label>Players (between 2-4)</label>
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Player 1" />
                        </div>
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Player 2" />
                        </div>
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Player 3" />
                        </div>
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Player 4" />
                        </div>
                    </div>
                    <button disabled={!this._isValid()} type="submit" className="btn btn-primary">Create game</button>
                </fieldset>
            </form>
        );
    },

    _isValid: function() {
        return (this.state.gameName) && this.state.players.length >= 2 && this.state.players.length <= 4;
    },

    // TODO fix
    _postCreateGameRequestToServer: function() {
        var self = this;
        var uri = SplendorWebAPIUtils.baseUrl('game');
        request.get(uri, function(res) {
            console.log('polled ' + uri + ' and got ' + JSON.stringify(res.body));
            if (self.isMounted()) {
                self.setState({games: res.body});
                setTimeout(self._updateStateFromServer, 2000);
            }
        });
    }
});

module.exports = CreateGame;