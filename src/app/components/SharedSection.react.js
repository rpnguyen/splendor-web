'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var TokenSection = require('./TokenSection.react');
var CardRowSection = require('./CardRowSection.react');
var NobleSection = require('./NobleSection.react');
var React = require('react');
var CardStore = require('../stores/CardStore');
var NobleStore = require('../stores/NobleStore');
var TokenStore = require('../stores/TokenStore');
var TurnStore = require('../stores/TurnStore');
var PlayerStore = require('../stores/PlayerStore');
var TurnUtils = require('../utils/TurnUtils');

function getStateFromStores() {
    return {
        cards: CardStore.getAll(),
        deckCounts: CardStore.getDeckCounts(),
        nobles: NobleStore.getAll(),
        tokens: TokenStore.getAll(),
        turnData: TurnStore.getTurnData(),
        user: PlayerStore.getUser()
    };
}

var SharedSection = React.createClass({

    getInitialState: function() {
        return {
            cards: [],
            deckCounts: {
                '1': 0,
                '2': 0,
                '3': 0
            },
            nobles: [],
            tokens: {
                'red': 0,
                'green': 0,
                'blue': 0,
                'white': 0,
                'black': 0,
                'gold': 0
            },
            turnData: {},
            user: {
                cards: [],
                reservedCards: [],
                tokens: {},
                nobles: []
            }
        };
    },

    componentDidMount: function() {
        CardStore.addChangeListener(this._onChange);
        NobleStore.addChangeListener(this._onChange);
        TokenStore.addChangeListener(this._onChange);
        TurnStore.addChangeListener(this._onChange);
        PlayerStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function() {
        CardStore.removeChangeListener(this._onChange);
        NobleStore.removeChangeListener(this._onChange);
        TokenStore.removeChangeListener(this._onChange);
        TurnStore.removeChangeListener(this._onChange);
        PlayerStore.removeChangeListener(this._onChange);
    },

    render: function() {
        return (
            <div className="shared panel panel-primary text-center">
                <div className="panel-body">
                    <TokenSection tokens={this.state.tokens} turnData={this.state.turnData}
                        shouldEnableTokens={this.shouldEnableTokens}
                        onClickHandler={this.tokenOnClickHandler}
                        tokenColourRestriction={this.tokenColourRestriction} />
                    <CardRowSection cards={this.state.cards} deckCounts={this.state.deckCounts}
                        turnData={this.state.turnData} user={this.state.user} />
                    <NobleSection nobles={this.state.nobles} turnData={this.state.turnData}
                        nobleRestriction={this.nobleRestriction} />
                </div>
            </div>
        );
    },

    shouldEnableTokens: function() {
        var enableForTakeTokensPhase = TurnUtils.inTakeTokensPhase(this.state.turnData)
            && TurnUtils.canTakeTokens(this.state.turnData, this.state.tokens);

        var enableForReservationPhase = TurnUtils.inReservationPhase(this.state.turnData)
            && !TurnUtils.canReserveCard(this.state.turnData, this.state.user)
            && TurnUtils.needsToTakeGold(this.state.turnData, this.state.tokens);

        return (enableForTakeTokensPhase || enableForReservationPhase);
    },

    tokenOnClickHandler: function(colour) {
        SplendorViewActionCreators.takeToken(colour);
    },

    tokenColourRestriction: function(colour, tokens) {
        var enableForTakeTokensPhase = TurnUtils.inTakeTokensPhase(this.state.turnData)
            && TurnUtils.canTakeColour(this.state.turnData, colour, tokens[colour]);

        var enableForReservationPhase = TurnUtils.inReservationPhase(this.state.turnData)
            && colour === 'gold';

        return (enableForTakeTokensPhase || enableForReservationPhase);
    },

    nobleRestriction: function(noble) {
        var endOfTakeTokensPhase = TurnUtils.inTakeTokensPhase(this.state.turnData)
            && !TurnUtils.canTakeTokens(this.state.turnData, this.state.tokens)
            && !TurnUtils.needsToDiscard(this.state.user.tokens);

        var endOfBuyCardPhase = TurnUtils.inBuyCardPhase(this.state.turnData)
            && !TurnUtils.needsToChooseCard(this.state.turnData)
            && !TurnUtils.needsToPay(this.state.turnData, this.state.user);

        var endOfReservationPhase = TurnUtils.inReservationPhase(this.state.turnData)
            && !TurnUtils.canReserveCard(this.state.turnData, this.state.user)
            && !TurnUtils.needsToTakeGold(this.state.turnData, this.state.tokens)
            && !TurnUtils.needsToDiscard(this.state.user.tokens);

        return (endOfTakeTokensPhase || endOfBuyCardPhase || endOfReservationPhase)
            && TurnUtils.needsToTakeNoble(this.state.turnData, this.state.user.cards, this.state.nobles)
            && TurnUtils.canAffordNoble(this.state.turnData, this.state.user.cards, noble);
    },

    _onChange: function() {
        this.setState(getStateFromStores());
    }

});

module.exports = SharedSection;
