'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var React = require('react');

var ReactPropTypes = React.PropTypes;

var Noble = React.createClass({

    getDefaultProps: function() {
        return {
            noble: {},
            nobleRestriction: function() {
                return false;
            }
        };
    },

    propTypes: {
        noble: ReactPropTypes.object,
        nobleRestriction: ReactPropTypes.func
    },

    render: function() {
        var noble = this.props.noble;
        if (noble.id === undefined) return <span />;

        var bgDivStyle = {
            backgroundImage: 'radial-gradient(circle at bottom left, #F8D030 0%, #FFFFFF 100%)'
        };
        var imageDivStyle = {
            backgroundImage: 'url(./assets/images/noble_' + noble.id + '.png)',
            backgroundSize: '120%',
            backgroundPosition: 'top'
        };

        var costBubbles = [];
        for (var colour in noble.cost) {
            if (noble.cost[colour] > 0) {
                costBubbles.push(
                    <div className={'btn btn-default cost-bubble ' + colour} key={colour}>
                    {noble.cost[colour]}
                    </div>
                );
            }
        }
        var enabled = this.props.nobleRestriction(this.props.noble);

        return (
            <button type="button"
                disabled={!enabled}
                className="btn btn-default card noble"
                onClick={this._onClick}>
                <div className="bg" style={bgDivStyle}>
                    <div className="image" style={imageDivStyle}></div>
                    <div className="points">{noble.points}</div>
                    <div className="cost btn-group" role="group">
                    {costBubbles}
                    </div>
                </div>
            </button>
        );
    },

    _onClick: function() {
        SplendorViewActionCreators.takeNoble(this.props.noble);
    }

});

module.exports = Noble;
