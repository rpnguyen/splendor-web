'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var Card = require('./Card.react');
var React = require('react');
var TurnUtils = require('../utils/TurnUtils');

var SplendorConstants = require('../constants/SplendorConstants');
var ActionTypes = SplendorConstants.ActionTypes;

var ReactPropTypes = React.PropTypes;

var StatusReservation = React.createClass({

    getDefaultProps: function() {
        return {
            turnData: {},
            user: {
                cards: [],
                reservedCards: [],
                tokens: {},
                nobles: []
            },
            nobles: [],
            tokens: {
                'red': 0,
                'green': 0,
                'blue': 0,
                'white': 0,
                'black': 0,
                'gold': 0
            }
        };
    },

    propTypes: {
        turnData: ReactPropTypes.object,
        user: ReactPropTypes.object,
        nobles: ReactPropTypes.array,
        tokens: ReactPropTypes.object
    },

    render: function() {
        var status;
        var turnData = this.props.turnData;
        var user = this.props.user;
        var tokens = this.props.tokens;
        var nobles = this.props.nobles;
        var undo = (<div className="btn-group pull-right" role="group">
            <button type="button" className="btn btn-default" onClick={SplendorViewActionCreators.undo}>
                <span className="fa fa-undo"></span>
            </button>
        </div>);
        if (TurnUtils.inReservationPhase(turnData)) {
            if (TurnUtils.canReserveCard(turnData, user)) {
                status = (
                    <div className="clearfix">
                    {undo}
                    Choose a card to reserve. Choose a deck to reserve the top card from that deck.
                    </div>
                );
            } else if (TurnUtils.needsToTakeGold(turnData, tokens)) {
                status = (
                    <div className="clearfix">
                    {undo}
                        Woo, grab a gold gem
                    </div>
                );
            } else if (TurnUtils.needsToDiscard(user.tokens)) {
                status = (
                    <div className="clearfix">
                    {undo}
                    You have too many gems! Discard some until you have only 10.
                    </div>
                );
            } else if (TurnUtils.needsToTakeNoble(turnData, user.cards, nobles)) {
                status = (
                    <div className="clearfix">
                    {undo}
                    <b>You earnt a visit from a noble!</b> Select one now
                    </div>
                );
            } else {
                status = (
                    <div className="clearfix">
                        <div className="btn-group pull-right" role="group">
                            <button type="button" className="btn btn-default" onClick={SplendorViewActionCreators.undo}>
                                <span className="fa fa-undo"></span>
                            </button>
                            <button type="button" className="btn btn-primary" onClick={SplendorViewActionCreators.confirmTurn}>
                                <span className="fa fa-check-square-o"></span> Done
                            </button>
                        </div>
                    All done.
                    </div>
                );
            }
        } else {
            status = <span />;
        }

        return status;
    }

});

module.exports = StatusReservation;
