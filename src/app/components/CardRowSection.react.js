'use strict';

var CardRow = require('./CardRow.react');
var React = require('react');

var ReactPropTypes = React.PropTypes;

var CardRowSection = React.createClass({

    getDefaultProps: function() {
        return {
            cards: [],
            deckCounts: {
                '1': 0,
                '2': 0,
                '3': 0
            },
            turnData: {},
            user: {
                cards: [],
                reservedCards: [],
                tokens: {},
                nobles: []
            }
        };
    },

    propTypes: {
        cards: ReactPropTypes.array,
        deckCounts: ReactPropTypes.object,
        turnData: ReactPropTypes.object,
        user: ReactPropTypes.object
    },

    render: function() {
        var cards = this.props.cards;
        var deckCounts = this.props.deckCounts;
        var turnData = this.props.turnData;
        var user = this.props.user;
        return (
            <div>
                <CardRow level={1} cards={cards} deckCounts={deckCounts} turnData={turnData} user={user} />
                <CardRow level={2} cards={cards} deckCounts={deckCounts} turnData={turnData} user={user} />
                <CardRow level={3} cards={cards} deckCounts={deckCounts} turnData={turnData} user={user} />
            </div>
        );
    }

});

module.exports = CardRowSection;
