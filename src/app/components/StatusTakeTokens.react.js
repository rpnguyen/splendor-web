'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var React = require('react');
var TurnUtils = require('../utils/TurnUtils');

var SplendorConstants = require('../constants/SplendorConstants');
var ActionTypes = SplendorConstants.ActionTypes;

var ReactPropTypes = React.PropTypes;

var StatusTakeTokens = React.createClass({

    getDefaultProps: function() {
        return {
            turnData: {},
            user: {
                cards: [],
                reservedCards: [],
                tokens: {},
                nobles: []
            },
            nobles: [],
            tokens: {
                'red': 0,
                'green': 0,
                'blue': 0,
                'white': 0,
                'black': 0,
                'gold': 0
            }
        };
    },

    propTypes: {
        turnData: ReactPropTypes.object,
        user: ReactPropTypes.object,
        nobles: ReactPropTypes.array,
        tokens: ReactPropTypes.object
    },

    render: function() {
        var status;
        var turnData = this.props.turnData;
        var tokens = this.props.tokens;
        var user = this.props.user;
        var nobles = this.props.nobles;
        var undo = (<div className="btn-group pull-right" role="group">
            <button type="button" className="btn btn-default" onClick={SplendorViewActionCreators.undo}>
                <span className="fa fa-undo"></span>
            </button>
        </div>);
        if (TurnUtils.inTakeTokensPhase(turnData)) {
            if (TurnUtils.canTakeTokens(turnData, tokens)) {
                status = (
                    <div className="clearfix">
                    {undo}
                        Take 3 different gems or (if there are 4 or more in the stack) 2 identical gems
                    </div>
                );
            } else if (TurnUtils.needsToDiscard(user.tokens)) {
                status = (
                    <div className="clearfix">
                    {undo}
                        You have too many gems! Discard some until you have only 10
                    </div>
                );
            } else if (TurnUtils.needsToTakeNoble(turnData, user.cards, nobles)) {
                status = (
                    <div className="clearfix">
                    {undo}
                        <b>You earnt a visit from a noble! </b>
                        Select one now
                    </div>
                );
            } else {
                status = (
                    <div className="clearfix">
                        <div className="btn-group pull-right" role="group">
                            <button type="button" className="btn btn-default" onClick={SplendorViewActionCreators.undo}>
                                <span className="fa fa-undo"></span>
                            </button>
                            <button type="button" className="btn btn-primary" onClick={SplendorViewActionCreators.confirmTurn}>
                                <span className="fa fa-check-square-o"></span>
                                Done
                            </button>
                        </div>
                        All done.
                    </div>
                );
            }
        } else {
            status = <span />;
        }

        return status;
    }

});

module.exports = StatusTakeTokens;
