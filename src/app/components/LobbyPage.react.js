'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var React = require('react');
var CurrentGameList = require('./CurrentGameList.react');
var CreateGame = require('./CreateGame.react');

var LobbyPage = React.createClass({
    render: function() {
        return (
            <div>
                <div className="col-md-9">
                    <h1>Current games</h1>
                    <CurrentGameList />
                </div>
                <div className="col-md-3">
                    <h2>Create game</h2>
                <div className="well well-sm">
                        <CreateGame />
                    </div>
                </div>
            </div>
        );
    }

});

module.exports = LobbyPage;