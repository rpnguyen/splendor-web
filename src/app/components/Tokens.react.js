'use strict';

var React = require('react');

var ReactPropTypes = React.PropTypes;

var Tokens = React.createClass({

    getDefaultProps: function() {
        return {
            colour: '',
            tokens: {
                'red': 0,
                'green': 0,
                'blue': 0,
                'white': 0,
                'black': 0,
                'gold': 0
            },
            turnData: {},
            isParentSectionEnabled: function() {
                return true;
            },
            onClickHandler: function() {
            },
            tokenColourRestriction: function() {
                return true;
            }
        };
    },

    propTypes: {
        colour: ReactPropTypes.string,
        tokens: ReactPropTypes.object,
        turnData: ReactPropTypes.object,
        isParentSectionEnabled: ReactPropTypes.func,
        onClickHandler: ReactPropTypes.func,
        tokenColourRestriction: ReactPropTypes.func
    },

    getInitialState: function() {
        return {
            coordinates: [] // 1 xy pair for each token of this colour
        };
    },

    // Handle initial state
    componentWillMount: function() {
        var count = this.props.tokens[this.props.colour];
        this.updateCoordinateStateWithCorrectNumberOfTokens(count);
    },

    // Handle on props change
    componentWillReceiveProps: function(nextProps) {
        var count = nextProps.tokens[nextProps.colour];
        this.updateCoordinateStateWithCorrectNumberOfTokens(count);
    },

    updateCoordinateStateWithCorrectNumberOfTokens: function(count) {
        // these represent the coordinates of tokens drawn on the screen
        var tokenCoordinates = this.state.coordinates;

        if (tokenCoordinates.length != count) {
            while (tokenCoordinates.length < count) {
                tokenCoordinates.push(this.generateCoordinatesForNextToken(60, 20));
            }
            while (tokenCoordinates.length > count) {
                tokenCoordinates.shift();
            }

            this.setState({
                coordinates: tokenCoordinates
            });
        }
    },

    // Uses Mitchell's best-candidate algorithm to ensure a random uniform distribution of coordinates
    generateCoordinatesForNextToken: function(maxX, maxY) {
        var currentCoords = this.state.coordinates;
        var rand = function() {
            return {
                x: Math.random() * maxX,
                y: Math.random() * maxY,
                rotate: Math.random() * 360
            };
        };

        // First one!
        if (currentCoords.length === 0) return rand();

        var getDistanceToNearest = function (candidate) {
            var shortest;
            for (var i = 0; i < currentCoords.length; i++) {
                var distance = getDistance(currentCoords[i], candidate);
                if (!shortest || distance < shortest) shortest = distance;
            }
            return shortest;
        };

        var getDistance = function (coord1, coord2) {
            var dx = Math.abs(coord1.x - coord2.x),
                dy = Math.abs(coord1.y - coord2.y),
                distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
            return Math.floor(distance);
        };

        var best, bestDistance;
        for (var i = 0; i < 10; i++) { // generate random points
            var candidate = rand();
            var distance;
            distance = getDistanceToNearest(candidate);
            if (!best || distance > bestDistance) {
                best = candidate;
                bestDistance = distance;
            }
        }
        return best;
    },

    render: function() {
        var parentSectionEnabled = this.props.isParentSectionEnabled();
        var canTakeColour = this.props.tokenColourRestriction(this.props.colour, this.props.tokens);
        var zero = this.props.tokens[this.props.colour] === 0;
        var enabled = parentSectionEnabled && canTakeColour && !zero;

        var img = [];
        var coordinates = this.state.coordinates;
        for (var i = 0; i < coordinates.length; i++) {
            var imgStyle = {
                position: 'absolute',
                left: coordinates[i].x,
                top: coordinates[i].y,
                transform: 'rotate(' + coordinates[i].rotate + 'deg)',
                outline: '1px solid transparent' // this fixes antialiasing problem caused by rotation
            };
            img.push(
                <img key={coordinates[i].x + '' + coordinates[i].y} className="token" style={imgStyle} src={'./assets/images/token_' + this.props.colour + '.png'} />
            );
        }
        return (
            <button
                type="button" className={'btn btn-default token-bucket ' + this.props.colour + (zero ? ' zero' : '')}
                disabled={!enabled}
                onClick={this.props.onClickHandler.bind(null, this.props.colour)}>
                <div className="bg">
                {img}
                </div>
            </button>
        );
    }

});

module.exports = Tokens;
