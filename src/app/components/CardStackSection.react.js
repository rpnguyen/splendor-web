'use strict';

var CardStack = require('./CardStack.react');
var React = require('react');

var ReactPropTypes = React.PropTypes;

var CardStackSection = React.createClass({

    getDefaultProps: function() {
        return {
            cards: []
        };
    },

    propTypes: {
        cards: ReactPropTypes.array
    },

    render: function() {
        var cards = this.props.cards;
        return (
            <div>
                <CardStack colour="white" cards={cards} />
                <CardStack colour="blue" cards={cards} />
                <CardStack colour="green" cards={cards} />
                <CardStack colour="red" cards={cards} />
                <CardStack colour="black" cards={cards} />
            </div>
        );
    }

});

module.exports = CardStackSection;
