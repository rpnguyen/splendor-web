'use strict';

var React = require('react');
var Card = require('./Card.react');
var TokenSection = require('./TokenSection.react');
var Noble = require('./Noble.react');

var HelpPage = React.createClass({
    render: function() {
        return (
            <div>
                <h1>How to play PokéSplendor</h1>

                <dl className="dl-horizontal shared">
                    <dt>Victory</dt>
                    <dd>
                            Be the first to get to 15 points (top-left of card)
                    </dd>


                    <div className="text-center">
                        <Card card={{
                            id: 46,
                            level: 2,
                            colour: 'blue',
                            cost: {'blue': 5},
                            points: 2
                        }} />
                        <Card card={{
                            id: 72,
                            level: 3,
                            colour: 'white',
                            cost: {'white': 3, 'red': 3, 'black': 6},
                            points: 4
                        }} />
                        <Card card={{
                            id: 82,
                            level: 3,
                            colour: 'red',
                            cost: {'green': 7},
                            points: 4
                        }} />
                        <Card card={{
                            id: 79,
                            level: 3,
                            colour: 'green',
                            cost: {'blue': 7, 'green': 3},
                            points: 5
                        }} />
                    </div>
                    <br />
                    <br />

                    <dt>On your turn (pick one)</dt>
                    <dd>

                        <ul className="fa-ul">
                            <li><span className="fa-li fa fa-dollar"></span> Take gems - 3 of different colours
                                <b> or </b>
                                2 of the same colour</li>
                            <li><span className="fa-li fa fa-shopping-cart"></span> Buy a card and pay the cost (bottom-left of card)</li>
                            <li><span className="fa-li fa fa-lock"></span> Reserve a card and take a gold gem</li>
                        </ul>
                    </dd>

                    <div className="text-center">
                        <TokenSection tokens={{
                            'red': 7,
                            'green': 7,
                            'blue': 7,
                            'white': 7,
                            'black': 7,
                            'gold': 5
                        }} />
                    </div>
                    <br />
                    <br />

                    <dt>Special rules</dt>
                    <dd>
                        <ul>
                            <li>You can't take 2 gems of the same color if there are three or less of that color</li>
                            <li>You can't have more than 10 tokens. If you're going to end with more than 10, discard down to 10</li>
                            <li>Nobles come to you automatically at the end of your turn. You may only take one noble per turn</li>
                        </ul>
                    </dd>

                    <div className="text-center">
                        <Noble noble={{
                            'id': 0,
                            'points': 3,
                            cost: {'white': 3, 'blue': 3, 'black': 3}
                        }} />
                        <Noble noble={{
                            'id': 2,
                            'points': 3,
                            cost: {'white': 3, 'red': 3, 'black': 3}
                        }} />
                        <Noble noble={{
                            'id': 3,
                            'points': 3,
                            cost: {'green': 4, 'red': 4}
                        }} />
                        <Noble noble={{
                            'id': 4,
                            'points': 3,
                            cost: {'green': 4, 'blue': 4}
                        }} />
                        <Noble noble={{
                            'id': 5,
                            'points': 3,
                            cost: {'red': 4, 'black': 4}
                        }} />
                    </div>

                </dl>


            </div>
        );
    },

    componentDidMount: function() {
    }

});

module.exports = HelpPage;