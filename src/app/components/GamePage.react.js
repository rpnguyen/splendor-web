'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var SharedSection = require('./SharedSection.react');
var StatusSection = require('./StatusSection.react');
var PlayerSection = require('./PlayerSection.react');
var React = require('react');

var GamePage = React.createClass({
    render: function() {
        return (
            <div className="row">
                <div className="col-md-6">
                    <StatusSection />
                    <SharedSection />
                </div>
                <div className="col-md-6">
                    <PlayerSection />
                </div>
            </div>
        );
    },

    componentDidMount: function() {
        SplendorViewActionCreators.pollServer();
    }
});

module.exports = GamePage;