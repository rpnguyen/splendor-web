'use strict';

var Tokens = require('./Tokens.react');
var React = require('react');

var ReactPropTypes = React.PropTypes;

var TokenSection = React.createClass({

    getDefaultProps: function() {
        return {
            tokens: {
                'red': 0,
                'green': 0,
                'blue': 0,
                'white': 0,
                'black': 0,
                'gold': 0
            },
            turnData: {},
            shouldEnableTokens: function() {
                return false;
            },
            onClickHandler: function() {
            },
            tokenColourRestriction: function() {
                return true;
            }
        };
    },

    propTypes: {
        tokens: ReactPropTypes.object,
        turnData: ReactPropTypes.object,
        shouldEnableTokens: ReactPropTypes.func,
        onClickHandler: ReactPropTypes.func,
        tokenColourRestriction: ReactPropTypes.func
    },

    render: function() {
        var tokens = this.props.tokens;
        var gold = [];
        if (tokens.gold > 0) {
            gold.push(
                <Tokens colour="gold" key="gold" tokens={tokens} turnData={this.props.turnData} isParentSectionEnabled={this.props.shouldEnableTokens} onClickHandler={this.props.onClickHandler} tokenColourRestriction={this.props.tokenColourRestriction} />
            );
            gold.push(<div key="spacer" className="clearfix"></div>);
        }
        return (
            <div>
                {gold}
                <Tokens colour="white" key="white" tokens={tokens} turnData={this.props.turnData} isParentSectionEnabled={this.props.shouldEnableTokens} onClickHandler={this.props.onClickHandler} tokenColourRestriction={this.props.tokenColourRestriction} />
                <Tokens colour="blue" key="blue" tokens={tokens} turnData={this.props.turnData} isParentSectionEnabled={this.props.shouldEnableTokens} onClickHandler={this.props.onClickHandler} tokenColourRestriction={this.props.tokenColourRestriction} />
                <Tokens colour="green" key="green" tokens={tokens} turnData={this.props.turnData} isParentSectionEnabled={this.props.shouldEnableTokens} onClickHandler={this.props.onClickHandler} tokenColourRestriction={this.props.tokenColourRestriction} />
                <Tokens colour="red" key="red" tokens={tokens} turnData={this.props.turnData} isParentSectionEnabled={this.props.shouldEnableTokens} onClickHandler={this.props.onClickHandler} tokenColourRestriction={this.props.tokenColourRestriction} />
                <Tokens colour="black" key="black" tokens={tokens} turnData={this.props.turnData} isParentSectionEnabled={this.props.shouldEnableTokens} onClickHandler={this.props.onClickHandler} tokenColourRestriction={this.props.tokenColourRestriction} />
            </div>
        );
    }

});

module.exports = TokenSection;
