'use strict';

var React = require('react');
var Colours = require('../utils/Colours');

var ReactPropTypes = React.PropTypes;

var Card = React.createClass({

    getDefaultProps: function() {
        return {
            card: {},
            shouldEnableCard: function(cost) {
                return false;
            },
            onClickHandler: function() {
            }
        };
    },

    propTypes: {
        card: ReactPropTypes.object,
        shouldEnableCard: ReactPropTypes.func
    },

    render: function() {
        var card = this.props.card;
        if (card.id === undefined || card.id === -1) // -1 represents the temporary placeholder card after buying from the board
            return <button type="button" disabled className={'btn btn-default card'}></button>;

        var bgDivStyle = {
            backgroundImage: 'radial-gradient(circle at bottom left, #FFFFFF 0%, ' + Colours.toHex(card.colour) + ' ' + (280 - card.level * 70) + '%)'
        };
        var id = (card.id < 10) ? '0' + card.id : card.id;
        var imageDivStyle = {
            backgroundImage: 'url(./assets/images/card_' + id + '.png)',
            backgroundSize: 'auto 80%'
        };
        var tokenSrc = './assets/images/token_' + card.colour + '.png';
        var points = (card.points > 0) ? <div className="points">{card.points}</div> : null;

        var costBubbles = [];
        for (var colour in card.cost) {
            if (card.cost[colour] > 0) {
                costBubbles.push(
                    <div
                        className={'btn btn-default cost-bubble ' + colour}
                        key={colour}>
                    {card.cost[colour]}
                    </div>
                );
            }
        }

        return (
            <button type="button"
                disabled={!this.props.shouldEnableCard(card.cost)}
                className={'btn btn-default card'}
                onClick={this.props.onClickHandler.bind(null, this.props.card)}>
                <div className="bg" style={bgDivStyle}>
                    <div className="image" style={imageDivStyle}></div>
                    <img className="token" src={tokenSrc} />
                {points}
                    <div className="cost btn-group-vertical" role="group">
                    {costBubbles}
                    </div>
                </div>
            </button>
        );
    }

});

module.exports = Card;
