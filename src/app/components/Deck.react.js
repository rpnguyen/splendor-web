'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var React = require('react');
var TurnUtils = require('../utils/TurnUtils');

var ReactPropTypes = React.PropTypes;

var Deck = React.createClass({

    getDefaultProps: function() {
        return {
            level: 0,
            deckCount: 0,
            turnData: {},
            user: {
                cards: [],
                reservedCards: [],
                tokens: {},
                nobles: []
            }
        };
    },

    propTypes: {
        level: ReactPropTypes.number,
        deckCount: ReactPropTypes.number,
        turnData: ReactPropTypes.object,
        user: ReactPropTypes.object
    },

    render: function() {
        if (this.props.deckCount === 0) return <button type="button" disabled className={'btn btn-default card'}></button>;

        var colour =
            (this.props.level === 1) ? '#FF1111'
                : (this.props.level === 2) ? '#FFD733'
                : '#A040A0';
        var imageDivStyle = {
            backgroundImage: 'url(./assets/images/deck_' + this.props.level + '.png)',
            backgroundSize: 'auto 40%'
        };
        return (
            <button type="button"
                disabled={!this._shouldEnableDeck()}
                className={'btn btn-default card no-border'}
                onClick={this._onClick}
                style={{backgroundColor: colour}}>
                <div className="bg">
                    <div className="image" style={imageDivStyle}></div>
                    <div className="deck-count">{this.props.deckCount}</div>
                </div>
            </button>
        );
    },

    _shouldEnableDeck: function() {
        return TurnUtils.inReservationPhase(this.props.turnData)
            && TurnUtils.canReserveCard(this.props.turnData, this.props.user);
    },

    _onClick: function() {
        SplendorViewActionCreators.reserveDeck(this.props.level);
    }

});

module.exports = Deck;
