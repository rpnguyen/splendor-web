'use strict';

var React = require('react');

var TurnStore = require('../stores/TurnStore');
var PlayerStore = require('../stores/PlayerStore');
var NobleStore = require('../stores/NobleStore');
var TokenStore = require('../stores/TokenStore');
var StatusWaiting = require('./StatusWaiting.react');
var StatusChooseAction = require('./StatusChooseAction.react');
var StatusTakeTokens = require('./StatusTakeTokens.react');
var StatusBuyCard = require('./StatusBuyCard.react');
var StatusReservation = require('./StatusReservation.react');
var StatusGameOver = require('./StatusGameOver.react');

var SplendorConstants = require('../constants/SplendorConstants');
var ActionTypes = SplendorConstants.ActionTypes;

function getStateFromStores() {
    return {
        currentPlayerName: TurnStore.getCurrentPlayerName(),
        turnData: TurnStore.getTurnData(),
        tokens: TokenStore.getAll(),
        user: PlayerStore.getUser(),
        players: PlayerStore.getAll(),
        nobles: NobleStore.getAll()
    };
}

var StatusSection = React.createClass({

    getInitialState: function() {
        return getStateFromStores();
    },

    componentDidMount: function() {
        TurnStore.addChangeListener(this._onChange);
        TokenStore.addChangeListener(this._onChange);
        PlayerStore.addChangeListener(this._onChange);
        NobleStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function() {
        TurnStore.removeChangeListener(this._onChange);
        TokenStore.removeChangeListener(this._onChange);
        PlayerStore.removeChangeListener(this._onChange);
        NobleStore.removeChangeListener(this._onChange);
    },

    render: function() {
        return (
            <div className="well well-sm">
                <StatusWaiting turnData={this.state.turnData} currentPlayerName={this.state.currentPlayerName} />
                <StatusChooseAction turnData={this.state.turnData} user={this.state.user} />
                <StatusTakeTokens turnData={this.state.turnData} user={this.state.user} nobles={this.state.nobles} tokens={this.state.tokens} />
                <StatusBuyCard turnData={this.state.turnData} user={this.state.user} nobles={this.state.nobles} />
                <StatusReservation turnData={this.state.turnData} user={this.state.user} nobles={this.state.nobles} tokens={this.state.tokens} />
                <StatusGameOver turnData={this.state.turnData} players={this.state.players} />
            </div>
        );
    },

    _onChange: function() {
        this.setState(getStateFromStores());
    }

});

module.exports = StatusSection;
