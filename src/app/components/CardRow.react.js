'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var Card = require('./Card.react');
var Deck = require('./Deck.react');
var React = require('react');
var TurnUtils = require('../utils/TurnUtils');
var PlayerUtils = require('../utils/PlayerUtils');

var ReactPropTypes = React.PropTypes;

var CardRow = React.createClass({

    getDefaultProps: function() {
        return {
            level: 0,
            cards: [],
            deckCounts: {
                '1': 0,
                '2': 0,
                '3': 0
            },
            turnData: {},
            user: {
                cards: [],
                reservedCards: [],
                tokens: {},
                nobles: []
            }
        };
    },

    propTypes: {
        level: ReactPropTypes.number,
        cards: ReactPropTypes.array,
        deckCounts: ReactPropTypes.object,
        turnData: ReactPropTypes.object,
        user: ReactPropTypes.object
    },

    render: function() {
        var cards = [];
        for (var card of this.props.cards) {
            if (card.level === this.props.level) {
                cards.push(<Card key={card.id} card={card} shouldEnableCard={this.shouldEnableCard} onClickHandler={this.cardOnClickHandler} />);
            }
        }
        return (
            <div>
                <Deck key={'deck' + this.props.level} level={this.props.level} deckCount={this.props.deckCounts[this.props.level]} turnData={this.props.turnData} user={this.props.user} />
            {cards}
            </div>
        );
    },

    shouldEnableCard: function(cost) {
        var enableForBuyCardPhase = TurnUtils.inBuyCardPhase(this.props.turnData)
            && TurnUtils.needsToChooseCard(this.props.turnData)
            && PlayerUtils.canAffordWithCardsAndTokens(this.props.user.cards, this.props.user.tokens, cost);

        var enableForReservationPhase = TurnUtils.inReservationPhase(this.props.turnData)
            && TurnUtils.canReserveCard(this.props.turnData, this.props.user);

        return (enableForBuyCardPhase || enableForReservationPhase);
    },

    cardOnClickHandler: function(card) {
        if (TurnUtils.inBuyCardPhase(this.props.turnData)) {
            SplendorViewActionCreators.buyCard(card);
        } else if (TurnUtils.inReservationPhase(this.props.turnData)) {
            SplendorViewActionCreators.reserveCard(card);
        }
    }

});

module.exports = CardRow;
