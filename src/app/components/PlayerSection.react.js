'use strict';

var Player = require('./Player.react');
var React = require('react');
var PlayerStore = require('../stores/PlayerStore');
var TurnStore = require('../stores/TurnStore');
var TokenStore = require('../stores/TokenStore');
var PlayerUtils = require('../utils/PlayerUtils');

var ReactPropTypes = React.PropTypes;

function getStateFromStores() {
    return {
        players: PlayerStore.getAll(),
        currentPlayerName: TurnStore.getCurrentPlayerName(),
        turnData: TurnStore.getTurnData(),
        tokens: TokenStore.getAll()
    };
}

function generatePlayer(player, currentPlayerName, turnData, tokens) {
    if (!player) return null;
    return (
        <Player
            key={player.name}
            player={player}
            currentPlayerName={currentPlayerName}
            turnData={turnData}
            tokens={tokens}
        />
    );
}

var PlayerSection = React.createClass({

    getInitialState: function() {
        return {
            players: [],
            currentPlayerName: '',
            turnData: {}
        };
    },

    componentDidMount: function() {
        PlayerStore.addChangeListener(this._onChange);
        TurnStore.addChangeListener(this._onChange);
        TokenStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function() {
        PlayerStore.removeChangeListener(this._onChange);
        TurnStore.removeChangeListener(this._onChange);
        TokenStore.removeChangeListener(this._onChange);
    },

    _onChange: function() {
        this.setState(getStateFromStores());
    },

    render: function() {
        var players = [];
        if (this.state.players.length > 0) {
            var usersTurnIndex = PlayerStore.getTurnIndex(PlayerUtils.getUsername()); // zero-based
            var playerCount = PlayerStore.getPlayerCount();
            for (var i = 0; i < playerCount; i++) {
                // For 4 players if you are player 2, output panels 2/3/4/1
                var indexOfNextPlayerBeginningWithTheUser = (i + usersTurnIndex) % playerCount;
                players.push(generatePlayer(
                    this.state.players[indexOfNextPlayerBeginningWithTheUser], this.state.currentPlayerName, this.state.turnData, this.state.tokens
                ));
            }
        }
        return (
            <div>
            {players}
            </div>
        );
    }

});

module.exports = PlayerSection;
