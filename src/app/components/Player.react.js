'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var TokenSection = require('./TokenSection.react');
var CardStackSection = require('./CardStackSection.react');
var NobleSection = require('./NobleSection.react');
var ReservedCardSection = require('./ReservedCardSection.react');
var BuyingPowerSection = require('./BuyingPowerSection.react');
var React = require('react');
var PlayerUtils = require('../utils/PlayerUtils');
var TurnUtils = require('../utils/TurnUtils');

var ReactPropTypes = React.PropTypes;

var Player = React.createClass({

    getDefaultProps: function() {
        return {
            player: {},
            currentPlayerName: '',
            turnData: {},
            tokens: {
                'red': 0,
                'green': 0,
                'blue': 0,
                'white': 0,
                'black': 0,
                'gold': 0
            }
        };
    },

    propTypes: {
        player: ReactPropTypes.object,
        currentPlayerName: ReactPropTypes.string,
        turnData: ReactPropTypes.object,
        tokens: ReactPropTypes.object
    },

    render: function() {
        var player = this.props.player;
        var turnData = this.props.turnData;
        var panelClass = 'panel ' + ((this.props.currentPlayerName === player.name) ? 'panel-primary' : 'panel-default');
        var icon = (PlayerUtils.isUser(player.name))
            ? <span className="fa fa-user"></span>
            : (this.props.currentPlayerName === player.name)
                ? <span className="fa fa-refresh fa-spin"></span>
                : null;
        var score = PlayerUtils.calculateScore(player);
        return (
            <div className={panelClass}>
                <div className="panel-heading">
                    {icon} {player.turnOrder}. {player.name} ({score} points)
                </div>
                <div className="panel-body text-center player-panel">
                    <TokenSection tokens={player.tokens} turnData={turnData} shouldEnableTokens={this.shouldEnableTokens}
                        onClickHandler={this.tokenOnClickHandler} tokenColourRestriction={this.tokenColourRestriction} />
                    <div className="player-stacks">
                        <CardStackSection cards={player.cards} />
                    </div>
                    <div className="player-nobles">
                        <NobleSection nobles={player.nobles} />
                    </div>
                    <div className="player-reserve">
                        <ReservedCardSection player={player} turnData={turnData} />
                    </div>
                    <div className="player-buying-power">
                        <BuyingPowerSection player={player} />
                    </div>
                </div>
            </div>
        );
    },

    shouldEnableTokens: function() {
        var enableForTakeTokensPhase = TurnUtils.inTakeTokensPhase(this.props.turnData)
            && !TurnUtils.canTakeTokens(this.props.turnData, this.props.tokens)
            && TurnUtils.needsToDiscard(this.props.player.tokens);

        var enableForBuyCardPhase = TurnUtils.inBuyCardPhase(this.props.turnData)
            && !TurnUtils.needsToChooseCard(this.props.turnData)
            && TurnUtils.needsToPay(this.props.turnData, this.props.player);

        var enableForReservationPhase = TurnUtils.inReservationPhase(this.props.turnData)
            && !TurnUtils.canReserveCard(this.props.turnData, this.props.player)
            && !TurnUtils.needsToTakeGold(this.props.turnData, this.props.tokens)
            && TurnUtils.needsToDiscard(this.props.player.tokens);

        return (enableForTakeTokensPhase || enableForBuyCardPhase || enableForReservationPhase) && PlayerUtils.isUser(this.props.player.name);
    },

    tokenOnClickHandler: function(colour) {
        if (TurnUtils.inTakeTokensPhase(this.props.turnData) || TurnUtils.inReservationPhase(this.props.turnData)) {
            SplendorViewActionCreators.returnToken(colour);
        }

        if (TurnUtils.inBuyCardPhase(this.props.turnData)) {
            SplendorViewActionCreators.payToken(colour);
        }
    },

    tokenColourRestriction: function(colour, tokens) {
        var enableForTakeTokensPhase = TurnUtils.inTakeTokensPhase(this.props.turnData);
        var enableForBuyCardPhase = TurnUtils.inBuyCardPhase(this.props.turnData)
            && !TurnUtils.needsToChooseCard(this.props.turnData)
            && TurnUtils.paymentNeedsColour(this.props.turnData, this.props.player.cards, colour);
        var enableForReservationPhase = TurnUtils.inReservationPhase(this.props.turnData);

        return (enableForTakeTokensPhase || enableForBuyCardPhase || enableForReservationPhase) && PlayerUtils.isUser(this.props.player.name);
    }

});

module.exports = Player;
