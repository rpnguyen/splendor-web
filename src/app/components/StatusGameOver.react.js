'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var React = require('react');
var TurnUtils = require('../utils/TurnUtils');
var PlayerUtils = require('../utils/PlayerUtils');

var SplendorConstants = require('../constants/SplendorConstants');
var ActionTypes = SplendorConstants.ActionTypes;

var ReactPropTypes = React.PropTypes;

var StatusGameOver = React.createClass({

        getDefaultProps: function() {
            return {
                turnData: {},
                players: []
            };
        },

        propTypes: {
            turnData: ReactPropTypes.object,
            players: ReactPropTypes.array
        },

        render: function() {
            var status;
            var turnData = this.props.turnData;
            var players = this.props.players;
            if (TurnUtils.gameComplete(turnData)) {
                status = [];
                players.sort(this.scoreAndCardsComparatorDescending);

                status.push(
                    <p className="lead">{players[0].name} is the winner!!!</p>
                );

                for (var i = 0; i < players.length; i++) {
                    var player = players[i];
                    status.push(
                        <span>
                    {i + 1} - {player.name} with {PlayerUtils.calculateScore(player)} points, {player.cards.length} cards
                            <br />
                        </span>
                    );
                }
            } else {
                status = <span />;
            }

            return (
                <div>
                {status}
                </div>
            );
        },

        scoreAndCardsComparatorDescending: function(playerA, playerB) {
            var pointsA = PlayerUtils.calculateScore(playerA);
            var pointsB = PlayerUtils.calculateScore(playerB);

            if (pointsA != pointsB) {
                return pointsB - pointsA;
            } else {
                return (playerB.cards.length - playerA.cards.length);
            }
        }

    }
);

module.exports = StatusGameOver;
