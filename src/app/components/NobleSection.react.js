'use strict';

var Noble = require('./Noble.react');
var React = require('react');

var ReactPropTypes = React.PropTypes;

var NobleSection = React.createClass({

    getDefaultProps: function() {
        return {
            nobles: [],
            nobleRestriction: function() {
                return false;
            }
        };
    },

    propTypes: {
        nobles: ReactPropTypes.array,
        nobleRestriction: ReactPropTypes.func
    },

    render: function() {
        var nobles = [];
        for (var noble of this.props.nobles) {
            nobles.push(<Noble key={noble.id} noble={noble} nobleRestriction={this.props.nobleRestriction} />);
        }
        return (
            <div>
            {nobles}
            </div>
        );
    }

});

module.exports = NobleSection;
