'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var React = require('react');
var request = require('superagent');
var Cookies = require('cookies-js');
var Router = require('react-router');
var Link = Router.Link;
var SplendorWebAPIUtils = require('../utils/SplendorWebAPIUtils');

var SplendorConstants = require('../constants/SplendorConstants');
var Keys = SplendorConstants.Keys;

var ReactPropTypes = React.PropTypes;

function createGame(game) {
    var buttons = [];
    for (var playerName of game.playerNames) {
        buttons.push(<Link to="game" key={playerName} className="btn btn-default" onClick={setUserCookie.bind(null, playerName)}>Play as {playerName}</Link>);
    }
    return (
        <div key={game.name} className="panel panel-default">
            <div className="panel-heading">{game.name}</div>
            <div className="panel-body">
                <div className="btn-group" role="group">
                    {buttons}
                </div>
            </div>
        </div>
    );
}

function setUserCookie(username) {
    Cookies.set(Keys.USERNAME, username);
}

var CurrentGameList = React.createClass({

    getInitialState: function() {
        return {
            games: []
        };
    },

    render: function() {
        var games = this.state.games;
        if (games.length === 0) return <div>No games created yet :(</div>;

        var rows = games.map(createGame);

        return (
            <div>
                {rows}
            </div>
        );
    },

    componentDidMount: function() {
        this._updateStateFromServer();
    },

    _updateStateFromServer: function() {
        var self = this;
        var uri = SplendorWebAPIUtils.baseUrl('game');
        request.get(uri, function(res) {
            console.log('polled ' + uri + ' and got ' + JSON.stringify(res.body));
            if (self.isMounted()) {
                self.setState({games: res.body});
                setTimeout(self._updateStateFromServer, 2000);
            }
        });
    }
});

module.exports = CurrentGameList;