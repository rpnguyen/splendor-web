'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var Card = require('./Card.react');
var React = require('react');
var TurnUtils = require('../utils/TurnUtils');

var SplendorConstants = require('../constants/SplendorConstants');
var ActionTypes = SplendorConstants.ActionTypes;

var ReactPropTypes = React.PropTypes;

var StatusBuyCard = React.createClass({

    getDefaultProps: function() {
        return {
            turnData: {},
            user: {
                cards: [],
                reservedCards: [],
                tokens: {},
                nobles: []
            },
            nobles: []
        };
    },

    propTypes: {
        turnData: ReactPropTypes.object,
        user: ReactPropTypes.object,
        nobles: ReactPropTypes.array
    },

    render: function() {
        var status;
        var turnData = this.props.turnData;
        var user = this.props.user;
        var nobles = this.props.nobles;
        var undo = (<div className="btn-group pull-right" role="group">
            <button type="button" className="btn btn-default" onClick={SplendorViewActionCreators.undo}>
                <span className="fa fa-undo"></span>
            </button>
        </div>);
        if (TurnUtils.inBuyCardPhase(turnData)) {
            if (TurnUtils.needsToChooseCard(turnData)) {
                status = (
                    <div className="clearfix">
                    {undo}
                    Choose a card to buy
                    </div>
                );
            } else if (TurnUtils.needsToPay(turnData, user)) {
                var card = turnData.card;
                status = (
                    <div className="clearfix">
                    {undo}
                    Pay some gems to receive your card. You're trying to buy:<br /> <Card key={card.id} card={card} />
                    </div>
                );
            } else if (TurnUtils.needsToTakeNoble(turnData, user.cards, nobles)) {
                status = (
                    <div className="clearfix">
                    {undo}
                        <b>You earnt a visit from a noble!</b> Select one now
                    </div>
                );
            } else {
                status = (
                    <div className="clearfix">
                        <div className="btn-group pull-right" role="group">
                            <button type="button" className="btn btn-default" onClick={SplendorViewActionCreators.undo}>
                                <span className="fa fa-undo"></span>
                            </button>
                            <button type="button" className="btn btn-primary" onClick={SplendorViewActionCreators.confirmTurn}>
                                <span className="fa fa-check-square-o"></span> Done
                            </button>
                        </div>
                    All done.
                    </div>
                );
            }
        } else {
            status = <span />;
        }

        return status;
    }

});

module.exports = StatusBuyCard;
