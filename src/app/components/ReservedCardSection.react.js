'use strict';

var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
var Card = require('./Card.react');
var Deck = require('./Deck.react');
var React = require('react');
var TurnUtils = require('../utils/TurnUtils');
var PlayerUtils = require('../utils/PlayerUtils');

var ReactPropTypes = React.PropTypes;

var ReservedCardSection = React.createClass({

    getDefaultProps: function() {
        return {
            player: {},
            turnData: {}
        };
    },

    propTypes: {
        player: ReactPropTypes.object,
        turnData: ReactPropTypes.object
    },

    render: function() {
        if (!this.props.player.reservedCards || this.props.player.reservedCards.length === 0) return <span />;

        var reservedCards = [];
        var isUser = (PlayerUtils.isUser(this.props.player.name));
        for (var reservedCard of this.props.player.reservedCards) {
            if (isUser // Hide non-user reserved cards
                && reservedCard.id > -1) { // Placeholder deck cards get an id of -1
                reservedCards.push(<Card key={'reserve' + reservedCard.id} card={reservedCard} shouldEnableCard={this.shouldEnableCard} onClickHandler={this.onClickHandler} />);
            } else {
                reservedCards.push(<Deck key={'reserve' + reservedCard.id} level={reservedCard.level} deckCount={1} />);
            }
        }
        return (
            <div className="well well-sm">
            {reservedCards}
            </div>
        );
    },

    shouldEnableCard: function(cost) {
        return PlayerUtils.isUser(this.props.player.name)
            && TurnUtils.inBuyCardPhase(this.props.turnData)
            && TurnUtils.needsToChooseCard(this.props.turnData)
            && PlayerUtils.canAffordWithCardsAndTokens(this.props.player.cards, this.props.player.tokens, cost);
    },

    onClickHandler: function(card) {
        SplendorViewActionCreators.buyCard(card);
    }

});

module.exports = ReservedCardSection;
