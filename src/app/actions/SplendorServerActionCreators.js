'use strict';

var PlayAudio = require('play-audio');
var Cookies = require('cookies-js');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var SplendorConstants = require('../constants/SplendorConstants');

var ActionTypes = SplendorConstants.ActionTypes;
var Keys = SplendorConstants.Keys;

module.exports = {

    gameSync: function(data) {
        AppDispatcher.handleServerAction({
            type: ActionTypes.GAME_SYNC,
            data: data
        });

        if (data && data.complete === true) {
            this.endGame();
        } else if (data && data.currentPlayer === Cookies.get(Keys.USERNAME)) {
            this.startTurn();
        } else {
            // This is actually a circular dependency so the require() is called late to allow proper object construction
            // -> SplendorViewActionCreators.pollServer
            // -> SplendorWebAPIUtils.getDataFromServer
            // -> SplendorServerActionCreators.gameSync
            //      -> IF it's not the user's turn yet...
            //      -> SplendorViewActionCreators.pollServer
            var SplendorViewActionCreators = require('../actions/SplendorViewActionCreators');
            setTimeout(SplendorViewActionCreators.pollServer, 1000);
        }
    },

    startTurn: function() {
        AppDispatcher.handleServerAction({
            type: ActionTypes.START_TURN
        });
        new PlayAudio('./assets/sounds/startturn.wav').play();
    },

    endTurn: function() {
        AppDispatcher.handleServerAction({
            type: ActionTypes.END_TURN
        });
        new PlayAudio('./assets/sounds/endturn.wav').play();
    },

    endGame: function() {
        AppDispatcher.handleServerAction({
            type: ActionTypes.END_GAME
        });
        new PlayAudio('./assets/sounds/victory.mp3').play();
    }
};
