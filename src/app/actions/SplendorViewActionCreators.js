'use strict';

var PlayAudio = require('play-audio');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var TurnStore = require('../stores/TurnStore');
var TurnUtils = require('../utils/TurnUtils');
var SplendorWebAPIUtils = require('../utils/SplendorWebAPIUtils');

var SplendorConstants = require('../constants/SplendorConstants');
var Keys = SplendorConstants.Keys;
var ActionTypes = SplendorConstants.ActionTypes;

module.exports = {

    pollServer: function() {
        SplendorWebAPIUtils.getDataFromServer();
    },

    startTakeTokens: function() {
        AppDispatcher.handleViewAction({
            type: ActionTypes.TAKE_TOKENS_PHASE
        });
    },

    takeToken: function(colour) {
        AppDispatcher.handleViewAction({
            type: ActionTypes.TAKE_TOKEN,
            colour: colour
        });
        new PlayAudio('./assets/sounds/token.wav').play();
    },

    returnToken: function(colour) {
        AppDispatcher.handleViewAction({
            type: ActionTypes.RETURN_TOKEN,
            colour: colour
        });
        new PlayAudio('./assets/sounds/token.wav').play();
    },

    startBuyCard: function() {
        AppDispatcher.handleViewAction({
            type: ActionTypes.BUY_CARD_PHASE
        });
    },

    buyCard: function(card) {
        AppDispatcher.handleViewAction({
            type: ActionTypes.BUY_CARD,
            card: card
        });
        new PlayAudio('./assets/sounds/card.wav').play();
    },

    payToken: function(colour) {
        AppDispatcher.handleViewAction({
            type: ActionTypes.PAY_TOKEN,
            colour: colour
        });
        new PlayAudio('./assets/sounds/token.wav').play();
    },

    takeNoble: function(noble) {
        AppDispatcher.handleViewAction({
            type: ActionTypes.TAKE_NOBLE,
            noble: noble
        });
        new PlayAudio('./assets/sounds/noble.mp3').play();
    },

    startReservation: function() {
        AppDispatcher.handleViewAction({
            type: ActionTypes.RESERVATION_PHASE
        });
    },

    reserveCard: function(card) {
        AppDispatcher.handleViewAction({
            type: ActionTypes.RESERVE_CARD,
            card: card
        });
        new PlayAudio('./assets/sounds/card.wav').play();
    },

    reserveDeck: function(level) {
        AppDispatcher.handleViewAction({
            type: ActionTypes.RESERVE_DECK,
            level: level
        });
        new PlayAudio('./assets/sounds/card.wav').play();
    },

    undo: function() {
        AppDispatcher.handleViewAction({
            type: ActionTypes.UNDO
        });
    },

    confirmTurn: function() {
        var turnData = TurnStore.getTurnData();
        var url = TurnUtils.getUrl(turnData);
        var payload = TurnUtils.createPayload(turnData);
        SplendorWebAPIUtils.postToServer(url, payload);
    }

};
