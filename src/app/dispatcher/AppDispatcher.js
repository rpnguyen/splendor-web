'use strict';

var SplendorConstants = require('../constants/SplendorConstants');
var Dispatcher = require('flux').Dispatcher;
var assign = require('object-assign');

var PayloadSources = SplendorConstants.PayloadSources;

var AppDispatcher = assign(new Dispatcher(), {

    handleServerAction: function(action) {
        console.log('server action: ' + JSON.stringify(action));
        this.dispatch({
            source: PayloadSources.SERVER_ACTION,
            action: action
        });
    },

    handleViewAction: function(action) {
        console.log('view action: ' + JSON.stringify(action));
        this.dispatch({
            source: PayloadSources.VIEW_ACTION,
            action: action
        });
    }

});

module.exports = AppDispatcher;